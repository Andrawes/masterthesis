% Andrawes Al Bahou (c) - ETH Zurich - 2018

% NOTE: This function is now deprecated

function isGranted = request_job(res_path, name, rf_line, depth, ang, thread_id)
    
    %==================================================================================
    % Defining key strings and variables used in communications with ReservationManager
    %==================================================================================
    % Job Request String to request a job reservation from ReservationManager
    job_request = strcat(res_path,'/add_',name,'_',num2str(rf_line),'_',num2str(depth),'_',num2str(ang),'_',thread_id,'.mat');
    
    % Acknowledgement expected from ReservationManager, if the reservation is approved
    job_ack = strcat(res_path,'/ack_',name,'_',num2str(rf_line),'_',num2str(depth),'_',num2str(ang),'_',thread_id,'.mat');
    
    % Regular expression selecting all the acknowledgements
    all_ack_re = strcat(res_path,'/ack_',name,'_',num2str(rf_line),'_',num2str(depth),'_',num2str(ang),'_*');

    % Other variables
    dummy_str = '';             % Empty string
    isGranted = false;          % Return flag 
    %==================================================================================

    

    %==================================================================================
    % Defining key strings used in communications with ReservationManager
    %==================================================================================

    % Search among all acknowledgement files with the corresponding reservation 
    all_ack_files = dir(all_ack_re)';

    % While no reservation acknowledgements are found
    while length(all_ack_files) == 0        
        % Send request (reservation) for a job
        save(job_request, 'dummy_str');
        % Pause execution for 1 second (to allow for ReservationManager to process it)       
        pause(1);     
        % Search again for corresponding acknowledgement files 
        all_ack_files = dir(all_ack_re)';
    end

    %Search among all acknowledgements 
    for file = all_ack_files
        % If acknowledgement corresponds to the current thread_id (I.E Successful request)
        if(strcmp(strcat(res_path,'/',file.name), job_ack)) 
            isGranted = true; 
        end
    end
    %==================================================================================
end