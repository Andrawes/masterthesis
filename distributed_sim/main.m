% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% Main function of the ultrasound simulation routine (built mainly on top of
% the FieldII US simulation program). When using this program in a distributed
% setting, make sure you are using it in compiled mode, as it requires no MATLAB
% licence.
% This function takes in some configuration flags which setup the running mode of 
% the simulator. Mainly, they determine whether to run in data generation
% mode, or scatterer generation mode. In the former, random pairs of
% {scatter-map; b-mode image} are generated. In the latter, bmode images
% are simulated for predefined scatterers. After configuring the simulator, 
% it enters an infinite loop where it requests simulation tasks from JobAllocator,
% then runs them. 
%
% INPUT: 
%   thread_id:  (string) ID of the current thread. This can be passed
%               as the first command line argument when running in 
%               compiled mode

function [] = main(thread_id)

    display(strcat('My job id is : ', thread_id));

    % ============================================================================================
    % General settings to configure execution mode. Other settings must be checked in "OptsInit.m"
    % ============================================================================================

    % Whether to simulate images or not
    simulate = true;
    % Number of different phantoms to simulate (deprecated).
    n_iters = 1;
    % Fast run divides the number of scatterers by 1000
    fast_run = false;
    % Dataset mode / evaluation_mode 
    evaluation_mode = true;
    % Whether to generate a round inclusion, or otherwise a random phantom 
    generate_incl = false;
    % Whether to generate scatter maps from CT images, or procedurally using random ellipsoids
    % This option does not matter if generate_incl is set to true.
    generate_from_ct = false;
    % 2D simulation or 3D volume simulation
    sim2d = true;
    % Randomly sample a new scatter pattern or use a specified sampling
    new_scatmap = true;
    % Whether to render the images after the rf lines have been simulated
    render_images = true;
    % When this flag is set to true, the angle at which to simulate the image is given in the
    % experiment name which must follow the following structure : expname^angle.exp and mustn't 
    % include underscores
    override_angles = false;
    % FieldII install path (Change according to your setup)
    f2_install_path = '/scratch_net/biwidl103/aalbahou/FieldII_install';
    % File containing the names of the experiements to run (actually currently this is a regular
    % expression pointing to a directory. We're using a multi-file system).
    exp_names_file = '/home/aalbahou/experiments/*.exp';
    % Reservation path
    res_path = '/home/aalbahou/reservations';
    % ============================================================================================



    % ============================================================================================
    % Matlab runtime options (Dependencies, Resource usage, Random seed)
    % ============================================================================================    
    
    % This if-protection prevents some dependency errors when running in compiled mode
    if (~isdeployed)
        % add Field II to the path
        addpath(genpath(f2_install_path));
        % addpath(genpath('D:\Users\FieldII_install')); % WINDOWS
    end

    % Set the maximum number of computational threads to 1.
    maxNumCompThreads(1);

    % Random restart the random number generator. (In MATLAB, the seed is the same on each restart)
    rng('shuffle');
    % ============================================================================================    



    % ============================================================================================    
    % Request a simulation job (from JobAllocator) then run it
    % ============================================================================================    

    % TODO*: Deprecate this and proceed to a sequential naming, or perhaps a single record file
    while(1)
    	% Request a simulation job for this thread
    	job = request_job_new(res_path, thread_id);
    	% Run the main routine
    	simulation_routine(simulate, fast_run, evaluation_mode, sim2d, new_scatmap, render_images, ...
                           job, override_angles, generate_incl, generate_from_ct);
    	
        % Send finished job signal
    	job_finished(res_path, thread_id);
    end
    % ============================================================================================    

    exit;

end % End function main    


    
    