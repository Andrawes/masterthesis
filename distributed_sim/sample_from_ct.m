% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function takes a regular 3D grid of points, and assigns them
% amplitudes. It does this by sampling existing medical image data (CT).
%
% INPUT: 
%   opts:       Options structure for this simulation.
%   x_size:     Lateral size of the phantom
%   y_size:     Elevation size of the phantom
%   z_size:     Axial size of the phantom 
%   z_start:    Distance of the phantom from the transducer
%   x_g:        Vector of x_coordinates of the points of the input grid
%   y_g:        Vector of y_coordinates of the points of the input grid
%   z_g:        Vector of z_coordinates of the points of the input grid
%
% OUTPUT: 
%   multiplicative_amplitude :  Vector of amplitudes assigned to the points
%                               of the input 3D grid


function multiplicative_amplitude = sample_from_ct(opts, x_size, y_size, z_size, z_start, x_g, y_g, z_g) 
    % ===========================================================================================================
    % Now we create the visual features by sampling CT data or by creating random ellipses
    % ===========================================================================================================

    % Load external volume
    %[vol, information] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig10_pv.mha');
    %dimensions = [0.625, 0.625, 0.301025]*1000 ; % In [mm]
    %[vol, information] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig11_pv.mha');
    % Dimensions [0.625, 0.625, 0.301025]
    %[vol, information] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig12_pv.mha');
    % Dimensions [0.625, 0.625, 0.300049]
    %[vol, information] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig13_pv.mha');
    % [0.625, 0.625, 0.300049]

    % Randomly chose which source file to fetch the feature data from   
    source_file = floor(rand()*(3));
    %source_file = 2;
    
    if (source_file == 0)
        [vol, information] = readMHA('/home/aalbahou_msc/data/shoulderCT3D/509961CT.mha');
        dimensions = [0.382813, 0.382813, 0.5] ;% In [m]
        
        % This zoom_out factor describes how much the scale of the scatterer
        % space is increased in order to match the scale in the CT space where
        % resolution is worse than in US.
        zoom_out_factor = floor(rand()*2)+3;
        %display('using volume 1')
    
    elseif (source_file == 1)
        [vol, information] = readMHA('/home/aalbahou_msc/data/visceralCT3D/10000005_1_CT_wb_superiorROI.mha');
        dimensions = [0.818359, 0.818359, 1.5] ; % In [m]
        zoom_out_factor = floor(rand()*3)+6;
        %display('using volume 2')
    else 
        [vol, information] = readMHA('/home/aalbahou_msc/data/visceralCT3D/10000005_1_CT_wb_superiorROI.mha');
        vol = permute(vol,[3,2,1]);
        dimensions = [1.5, 0.818359, 0.818359] ; % In [m]
        zoom_out_factor = floor(rand()*3)+4;
        %display('using volume 3')
    end

    [x_dim z_dim y_dim] = size(vol); % Resolution in pixels
    delta_pix_x = dimensions(1)/x_dim; % Distance between pixels in [m] (x-axis)
    delta_pix_z = dimensions(2)/z_dim; % Distance between pixels in [m] (z-axis)
	delta_pix_y = dimensions(3)/y_dim; % Distance between pixels in [m] (y-axis)    

    % Find the maximum offset of pixel positions in the circle in each dimension 
    right_edge_offset = round(zoom_out_factor*(max(x_g(:))+(x_size/2))/delta_pix_x)+1;
    bottom_edge_offset = round(zoom_out_factor*(max(z_g(:))-z_start)/delta_pix_z)+1;
    front_edge_offset = round(zoom_out_factor*(max(y_g(:))+(y_size/2))/delta_pix_y)+1;

    % Just making sure that an offset can actually be found.
    if (bottom_edge_offset >= length(vol(:,1,1)) || right_edge_offset >= length(vol(1,:,1)))
        display('You might need to reconsider the zoom out factor');
        display(bottom_edge_offset)
        display(length(vol(:,1,1)))
        display(right_edge_offset)
        display(length(vol(1,:,1)))
        %NEW
        display(front_edge_offset)
        display(length(vol(1,1,:)))
    end

    % Find Random window offsets
    window_offset_x = round(rand()*(length(vol(1,:,1))-right_edge_offset));        
    window_offset_z = round(rand()*(length(vol(:,1,1))-bottom_edge_offset));
    window_offset_y = round(rand()*(length(vol(1,1,:))-front_edge_offset));


    % Sample the CT volume to create the high-level features (in 2D)
    features = vol(sub2ind(size(vol),...
    		   			  round(zoom_out_factor*(z_g-z_start)/delta_pix_z)+1+window_offset_z,...
    		   			  round(zoom_out_factor*(x_g+(x_size/2))/delta_pix_x)+1+window_offset_x,...
						  round(zoom_out_factor*(y_g+(y_size/2))/delta_pix_y)+1+window_offset_y));


    % Scale features such that their values (amplitudes) are between 0 and 1
    maximum = double(max(vol(:)));
    minimum = double(min(vol(:)));
    features = double(features);
    features = (features-minimum)/(maximum-minimum);

    multiplicative_amplitude = features;
    % ===========================================================================================================

end % End function sample from CT 