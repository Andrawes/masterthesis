% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% 
% This short script scans through a 3D volume of a medical MHA file. It steps
% through (and displays) a movie of the view moving through the 3D body.
%

% Select a volume

[vol, inform] = readMHA('/home/aalbahou_msc/data/visceralCT3D/10000005_1_CT_wb_superiorROI.mha');
%[vol, inform] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig10_pv.mha');
%[vol, infor] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig11_pv.mha');
%[vol, infor] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig12_pv.mha');
%[vol, inform] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig13_pv.mha');
%[vol,inform] = readMHA('/home/aalbahou_msc/data/shoulderCT3D/509961CT.mha');
%[vol,inform] = readMHA('/home/aalbahou_msc/data/liverUS3D/SMT-01-0001.mha');
%[vol,inform] = readMHA('/home/aalbahou_msc/data/liverUS3D/SMT-07-0001.mha');
%[vol,inform] = readMHA('/home/aalbahou_msc/data/liverUS3D/EMC-06-1-0001.mha');
%[vol,inform] = readMHA('/home/aalbahou_msc/data/liverUS3D/EMC-07-1-0001.mha');

% Obtain volume dimensions
[x_dim y_dim z_dim] = size(vol);

% Find min and max
maximum = max(vol(:));
minimum = min(vol(:));

% Flip the volume
vol = permute(vol, [3,2,1]);
%vol = permute(vol, [2,3,1]);
%vol = permute(vol, [1,3,2]);

figure
for i = 1:z_dim-10
    imagesc(vol(:,:,i), [minimum maximum])
    pause(0.01)
end
