% Andrawes Al Bahou (c) - ETH Zurich - 2018

% FUNCTIONALITY:
% This function creates a cyst scatter map. It is a circular inclusion of lesser amplitude.
%
% INPUT: 
%   opts:               Options structure for this simulation
%   fast_run:           Flag. Used in debugging for running the US simulation quickly
%   evaluation_mode:    Flag. If true, means the program is in evaluation mode, else
%                       in dataset generation mode. 
%   sim2d:              Flag. If true, indicates the program is a 2D simulation (not 3D)
%   new_scatmap:        Flag. If true, indicates that a new random scatter-map can be generated.
%
% OUTPUT: 
%   x_s:                Vector of x-coordinates of the scatterers
%   y_s:                Vector of y-coordinated of the scatterers
%   z_s:                Vector of z_coordinated of the scatterers
%   amplitudes:         Vector of amplitudes of the scatterers

function [x_s, y_s, z_s, amplitudes] = make_cyst(opts, fast_run, evaluation_mode, sim2d, new_scatmap)

    % ==================================================================================================
    % Now we create randomly placed scatterers
    % ==================================================================================================    
    x_size = opts.x_size;           %  Width of phantom [m]
    y_size = opts.y_size;           %  Transverse width of phantom [m]
    z_size = opts.z_size;           %  Height of phantom [m]
    z_start = opts.z_start;         %  Start of phantom surface [m];
    if (fast_run == true)
        nbrSpeckles = opts.N/1000;  %  Number of speckles
    else
        nbrSpeckles = opts.N;       %  Number of speckles
    end

    % Create the speckle positions randomly
    x_s = rand(round(nbrSpeckles),1)*x_size/opts.x_scale_factor-(x_size/opts.x_scale_factor)/2;
    y_s = rand(round(nbrSpeckles),1)*y_size-y_size/2;
    z_s = rand(round(nbrSpeckles),1)*z_size/opts.z_scale_factor+z_start+opts.circ_offset_z;

    % Distance (in the Z-axis) between the inscribed rectangle, and outscribed square.    
    circ_offset_z = opts.circ_offset_z;

    % density coef such that {x,y,z}_levels = density_coef * {x,y,z}_size; 
    density_coef = (nbrSpeckles/(x_size*y_size*z_size))^(1/3);
    
    % Compute average separation distance between 2 neighboring grid vertices in [mm]
    delta = 1/(density_coef);
    delta_x = delta;
    delta_y = delta;
    delta_z = delta;
    % ==================================================================================================

    
    
    % ==================================================================================================
    % Create discretization grid (positions)
    % ==================================================================================================    
    
    % Width of the discretization grid on each axis
    %width_x = 109;
    width_x = 153;
    width_z = 192;
    width_y = 3;
    if sim2d == true
        width_y = 2;
    else
        width_y = 3;
    end

    % Increments in [mm] between adjacent grid vertices
    delta_xg = (x_size/opts.x_scale_factor)/(width_x-1);
    delta_yg = y_size/(width_y-1);
    delta_zg = (z_size/opts.z_scale_factor)/(width_z-1);   
    % Grid axes
    grid_base_x = (-(x_size/opts.x_scale_factor)/2:delta_xg:(x_size/opts.x_scale_factor)/2)'; 
    grid_base_y = (-(y_size)/2:delta_yg:(y_size)/2)';
    grid_base_z = (z_start+opts.circ_offset_z+(0:delta_zg:(z_size/opts.z_scale_factor))');
    % Discretization factor
    discretization_factor_x = delta_xg/delta;
    discretization_factor_y = delta_yg/delta;
    discretization_factor_z = delta_zg/delta;
    
    % Obtain indexes of grid of points with basis vectors grid_base_{x,y,z}
    [x_g, z_g, y_g] = meshgrid(grid_base_x, grid_base_z, grid_base_y);
    % ==================================================================================================



    % ==================================================================================================
    % Set the amplitudes (features) of the discretization grid. In evaluation mode, they are created 
    % based on the outputs of the neural network. In dataset mode, they are created randomly using 
    % ellipsoids.
    % ==================================================================================================

    % Initialize features matrix
    features = zeros(width_z, width_x, width_y);
    
    
    % In dataset generation mode 
    % ==============================================================================================
    % Assign the aplitudes by generating random ellipsoids in space
    % ==============================================================================================
    
    % Interpolation grid amplitudes
    % multiplicative_amplitude = make_ellipsoids(opts, x_size, y_size, z_size, z_start, x_g, y_g, z_g);
    
    cyst_radius = 7/1000;
    cyst_c_x = opts.r_c_x;
    cyst_c_z = opts.r_c_z + 4/1000;

    multiplicative_amplitude = ones(1,numel(x_g));
    cyst_mask = (((x_g-cyst_c_x).^2 + (z_g-cyst_c_z).^2 ).^(1/2) <= cyst_radius+1e-7);
    multiplicative_amplitude(cyst_mask) = 0.1;

    % Here the speckle noise is created by modulating the standard deviation and the mean.
    % TODO: We could try modulating the mean only, or the standard deviation only.
    % Create underlying speckle noise
    amplitudes_mean = 0.5;
    amplitudes_stdev = 0.3;
    feature_dims = size(multiplicative_amplitude);
    features = amplitudes_stdev.*randn(feature_dims(1),feature_dims(2))+ amplitudes_mean;
    %features = ones(feature_dims(1),feature_dims(2));
    features = multiplicative_amplitude.*features;
     
    % Clip scatterers with amplitudes larger than 1 or smaller than 0
    top_mask = features>1;
    bottom_mask = features <0;
    features(top_mask) = 1;
    features(bottom_mask) = 0;
     
    features = double(features);
    %features = (features-minimum)/(maximum-minimum);
       
    % This exception for the 2D scenario means that the values of the interpolation grid are constant
    % across depth (y-direction)
    % TODO: Cleanup this code possibly by merging it with similar if clause close to line 129
    if sim2d==true
        % Reshape into grid
        ft_dims = size(x_g);
        features = reshape(features, ft_dims(1), ft_dims(2), ft_dims(3));
        % make the interpolation grid depth-wise constant
        features(:,:,2) = features(:,:,1);
    end
    % ==============================================================================================





    % ==================================================================================================
    % Assign the scatter map amplitudes based on the interpolation grid
    % ==================================================================================================

    % Reshape into grid
    pixel_dims = size(x_g);
    pixels = reshape(features, pixel_dims(1), pixel_dims(2), pixel_dims(3));

    % Trilinear interpolation of the speckle amplitudes from the discrete grid
    amplitudes = interp3(x_g, z_g, y_g, pixels, x_s, z_s, y_s);
   
    % Create a circular mask
    % The 1e-7 was added to solve a small numerical problem whereby two small floats 
    % should be equal in theory but are not in practice
    circle_mask =  (((x_s-opts.r_c_x).^2 + (z_s-opts.r_c_z).^2 ).^(1/2) <= x_size/2+1e-7);
    % Remove all scatterers which are not in the circle
    x_s = x_s(circle_mask);
    y_s = y_s(circle_mask);
    z_s = z_s(circle_mask);
    amplitudes = amplitudes(circle_mask);

    % Pack the scatterer coordinate information into a single matrix
    positions = [x_s, y_s, z_s];

    % Save scatterer data 
    cmd=['save ',opts.scatter_map,'/scatterer_data.mat x_s y_s z_s amplitudes'];
    eval(cmd);

    % plot phantom
    plotPhantom(positions, amplitudes, opts.x_size, opts.z_size,opts.z_start, 'Speckle Phantom', opts.imgPath);
    % ==================================================================================================
           
           
           
    % ==================================================================================================
    % Write out the images
    % ==================================================================================================     

    % Rectangle mask to take into account only the rectangular region which will be used
    rectangle_mask = (x_g >= (-x_size/opts.x_scale_factor)/2 & x_g <= (x_size/opts.x_scale_factor)/2);
    rectangle_mask = rectangle_mask & ((z_g >= circ_offset_z+z_start) & (z_g <= circ_offset_z+z_start+z_size/opts.z_scale_factor) );
    
    % Select a rectangular subset of the scatterers to output into an image file
    pixels = pixels(rectangle_mask);
 
    % These calculations come from the written down sheet (Archive1) which also contains
    % references to the variable names used here
    Q = -(x_size/opts.x_scale_factor)/2-min(grid_base_x(:));
    R = (x_size/opts.x_scale_factor)/2-min(grid_base_x(:));
    
    if (Q/delta_xg == floor(Q/delta_xg))
        no1 = Q/delta_xg;
    else
        no1 = floor(Q/delta_xg)+1;
    end
    nk = floor(R/delta_xg)+1;
    ni = nk-no1;

    % Rehsape pixels into 2D image (with multiple channels)
    pixels = reshape(pixels, [], ni, length(grid_base_y));

    % Save individually all channels
    for c = 1:length(grid_base_y)
        % Write the resulting image to the output file

        location_name = convertStringsToChars(strcat(opts.phtPath,opts.experiment_name,'c',num2str(c),'.png'));
        imwrite(pixels(:,:,c),location_name);
    end
    % ==================================================================================================

end
