% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function takes a number of RF lines, places them together,
% and performs some additional processing to extract a B-mode image, 
% or an RF-image, and saves them.
%
% INPUT: 
%   opts:           Options structure for this simulation
%   probe_angle:    Angle in radians of US beam incidence
%   y_slice:        Depth of the current slice when simulating a 3D volume
%   render_bmode:   Flag: if true, renders a bmode image, else renders RF image

function [] = make_image(opts, probe_angle, y_slice, render_bmode)

    % =============================================================================================================================
    % General Settings
    % =============================================================================================================================

    fs = opts.fs;                   %  Sampling frequency [Hz]
    c = opts.c;                     %  Speed of sound [m/s]
    no_lines = opts.no_lines;       %  Number of elements for transmitting
    initial_time = 0.58162e-6;      %  Initial time (at which the transducer elements begin receiving)
    % =============================================================================================================================



    % =============================================================================================================================
    % Compute the minimum time tstart. This number is given by the Field II simulator for each RF line. We need to find the minimum
    % tstart time in order to reconstruct the bmode image from individual RF lines.
    % =============================================================================================================================
   
    % Initialize the values of tstart_array
    tstart_array = ones(no_lines,1)*9999999;
    %Determine the value of min_tstart (while also checking if all the rf_lines are present)
    for i=1:no_lines
        while ~exist([opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/rf_pos_',num2str(y_slice),'_ln_',num2str(i),'.mat'],'file')
            pause(30); % Wait for a few seconds (for other threads to finish computing the rf line)
        end
        load([opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/rf_pos_',num2str(y_slice),'_ln_',num2str(i),'.mat']);
        tstart_array(i) = tstart;
        
    end
    % Find minimum value of tstart
    min_tstart = min(tstart_array);

    % read the data and adjust it in time
    min_sample = min_tstart*fs;
    % =============================================================================================================================



    % Check if this particular image has already been computed, otherwise skip
    if ~exist(strcat(opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/rf_mat_',num2str(y_slice),'.mat'))    

        % =========================================================================================================================
        % Begin the reconstrction of the BMODE or RF image
        % =========================================================================================================================

        % Iterate over rf lines
        for i=1:no_lines
            % load rf lines
            load([opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/rf_pos_',num2str(y_slice),'_ln_',num2str(i),'.mat']);
            
            % If the minimum beam arrival time (min_sample) is negative, it must be corrected for to avoid Bmode image distortions
            if min_sample <0 
                rf = [zeros(round(tstart*fs-min_sample),1); rf_data];
            else 
                rf = [zeros(round(tstart*fs),1); rf_data];
            end
            rf_mat(1:max(size(rf)),i) = rf;
        end
        
        if min_sample <0
            rf_mat = rf_mat(round(abs(min_sample)):end,:);
        end
        
        % Save the collection of RF lines as a single matrix
        save(strcat(opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/rf_mat_',num2str(y_slice),'.mat'), 'rf_mat')
        
        





        gains = [20:1:30];
        offsets = [0.01 0.02 0.03]; 
        gains = [20];
        offsets = [0 0.01];

        for mgain = gains
        for offset = offsets

            % Chose to process raw RF data or Bmode data
            if (render_bmode == false)
                us_image = rf_mat;
            else 
                % Find signal envelope and perform logarithmic compression
                env = abs(hilbert(rf_mat));
                bmode = mgain*log10(env./max(env(:))+offset);
                us_image = bmode;
            end
           
            % plotting
            %f = figure;
            %image_width = opts.x_scale_factor*40e-3;               %  Size of image sector
            %d_x = image_width/no_lines;                            %  Increment for image
            %vrange = [-40 0];
            %x_axis = -image_width/2 + ((1:no_lines)-1)*d_x;
            %x_lim = [min(x_axis) max(x_axis)]*1e3;
            %z_lim = [opts.z_start opts.z_size+opts.z_start]*1e3;
            
            time = (0:(size(us_image,1)-1)).'/fs - initial_time;    % Z-axis in units of time
            z_axis = time*c/2;                                        % Z-axis in units of space
    
            % Crop
            first_pix = ceil(length(us_image)*opts.z_start/max(z_axis));
            last_pix = floor(length(us_image)*(opts.z_start+opts.z_size)/max(z_axis));
            us_image = us_image(first_pix:last_pix,:);
            
            % Cutoff bmode signal below -40dB
            if render_bmode == true
                if (opts.db40==true)
                    us_image(us_image<-40) = -40; 
                end
            end
    
            % Scale the image values
            us_image=us_image-min(us_image(:)); % shift data such that the smallest element of A is 0
            us_image=us_image/(max(us_image(:))-min(us_image(:))); % normalize the shifted data to 1 
            % =========================================================================================================================
    
    
    
            % =========================================================================================================================
            % Having obtained the full bmode image, we now want to extract a rectangular 
            % (rotated) portion
            % =========================================================================================================================
            
            % Output image: Scale in real [mm] space
            x_length_output = opts.x_size/opts.x_scale_factor;
            z_length_output = opts.z_size/opts.z_scale_factor;
        
            % Scale in pixel space (the scale will actually be one pixel smaller
            % in each dimension: discrepancy arises when generating the basis vectors)
            n_cols_output = 2*round(opts.no_lines/opts.x_scale_factor) ;
            n_rows_output = 2*round((opts.no_lines/opts.x_scale_factor)*z_length_output/x_length_output);
        
            % delta_z_output, and delta_x_output are the pixel height and width in the output image
            delta_z_output = z_length_output/(n_rows_output);
            delta_x_output = x_length_output/(n_cols_output); 
        
            % Basis vectors
            x_basis = -x_length_output/2:delta_x_output:x_length_output/2 ;
            z_basis = opts.z_start+opts.circ_offset_z+(0:delta_z_output:z_length_output);
        
            % Initialize the matrix containing the {x,z}_coordinates in 
            % real space [mm] of each pixel of the output image
            x_output = ones(length(z_basis), length(x_basis));
            z_output = ones(length(z_basis), length(x_basis));
            x_output = x_output.*x_basis ;
            z_output = z_output.*z_basis' ;
        
            % Flatten
            x_output = reshape(x_output,[1,length(x_basis)*length(z_basis)]);
            z_output = reshape(z_output,[1,length(x_basis)*length(z_basis)]);
        
            % Center of rotation ([mm],[mm])
            r_c = opts.r_c;
            r_c_x = r_c(1,:);
            r_c_y = r_c(2,:);
            r_c_z = r_c(3,:);
        
            % Rotation matrix 
            R_phi = [cos(probe_angle), -sin(probe_angle); sin(probe_angle), cos(probe_angle)];
        
            % Map the position vectors of the pixels in the output image
            % to position vectors in the input image. (Real space [mm])
            output_positions = R_phi*[x_output-r_c_x;z_output-r_c_z] ;
            x_input = output_positions(1,:)+r_c_x;
            z_input = output_positions(2,:)+r_c_z;
        
            % Input image:
            % Scale in real [mm] space
            x_length_input = opts.x_size;
            z_length_input = opts.z_size;
        
            % Scale in pixel space
            [n_rows_input n_cols_input] = size(us_image);
        
            % delta_z_input, and delta_x_input are the pixel height and width in the input image
            delta_z_input = z_length_input/n_rows_input;
            delta_x_input = x_length_input/n_cols_input;
        
            % The coordinates of the mapped vectors (from output to input) in pixel space
            coordinate_x = round((x_input+x_length_input/2)/delta_x_input);
            coordinate_z = round((z_input-opts.z_start)/delta_z_input);
        
            % Retrieve the pixel values from the bmode images 
            % (using subscript indexing to linear indexing conversion)
            values = us_image(sub2ind(size(us_image),coordinate_z,coordinate_x));
            
            % Reshape into 2D matrix
            values = reshape(values, [length(z_basis), length(x_basis)]);
            % Normalize values    
            values = (values - min(values(:)))/(max(values(:))-min(values(:)));
            % =========================================================================================================================
    
    
            % Display
            figure 
            imshow(values);
        
            % =========================================================================================================================
            % Save the output files
            % =========================================================================================================================
            % If the render_bmode switch is set, then save the bmode image, otherwise, save the rf image.
            if (render_bmode==true)
                fname = convertStringsToChars(strcat(opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/',opts.experiment_name,'bmode',num2str(y_slice),'_',num2str(probe_angle*360/(2*pi)),'_',num2str(mgain),'_',num2str(offset),'.png'));
    	    imwrite(values,fname,'png');
            else
                fname = convertStringsToChars(strcat(opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/',opts.experiment_name,'rf',num2str(y_slice),'_',num2str(probe_angle*360/(2*pi)),'.png'));
                imwrite(values,fname,'png');
            end
        % =========================================================================================================================
    
        end % Belongs to the for loop, for optimizing over the appearance of the image
        end % Belongs to the for loop, for optimizing over the appearance of the image

    end % End if    
end % End function make_image
