% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% Function for Andrawes's own use. Allows the viewing of
% US images from a US ".b8" type of file.

% File loctions
filename1='eval_data/steer_10mhz_0';
filename2='eval_data/steer_10mhz_-1';
filename3='eval_data/steer_10mhz_1';

% Read the RF-images
%[hdr(1) rf_raw(1).rf]=read_b8(strcat(filename1,'.rf'));
%[hdr(2) rf_raw(2).rf]=read_b8(strcat(filename2,'.rf'));
%[hdr(3) rf_raw(3).rf]=read_b8(strcat(filename3,'.rf'));


% the B-mode images
[img32_1]=readUSonix(strcat(filename1,'.b8'));
[img32_2]=readUSonix(strcat(filename2,'.b8'));
[img32_3]=readUSonix(strcat(filename3,'.b8'));


% Display the images
f = figure
colormap(gray(127));
subplot(1,3,1); image(img32_3(:,:)); title('B-mode, -10'); axis('image');
subplot(1,3,2); image(img32_1(:,:)); title('B-mode,   0'); axis('image');
subplot(1,3,3); image(img32_2(:,:)); title('B-mode, +10'); axis('image');
% saveas(f, 'my_figure.png')

% Display the images
f2 = figure
colormap(gray(127));
subplot(1,3,1); image(img32_3(79:400,200:457)); title('B-mode, -10'); axis('image');
subplot(1,3,2); image(img32_1(79:400,200:457)); title('B-mode,   0'); axis('image');
subplot(1,3,3); image(img32_2(79:400,200:457)); title('B-mode, +10'); axis('image');
saveas(f2, 'my_figure.png')

my_image = img32_1(80:400,201:457);
my_image = double(my_image);
my_image = my_image/(max(my_image(:)));

imwrite(my_image, 'rotation_0.png', 'png')



