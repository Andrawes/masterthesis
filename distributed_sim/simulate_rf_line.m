% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY : 
% This function is in charge of simulating a single RF line.
%
% INPUT : 
%   opts:           Options structure for this simulation
%   probe_angle:    Angle in radians of US beam incidence
%   line_no:        The number of the RF-line to be simulated 
%   amplitudes:     Vector of amplitudes of the scatterers in the scatter map
%   positions:      A N-by-3 matrix of the (x,y,z) coordinates of the N scatterers
%   y_slice:        Depth of the current slice when simulating a 3D volume
%
% OUTPUT:
%   Tx_aperture:    FieldII probe setting (Receive Aperture)
%   Rx_aperture:    FieldII probe setting (Transmit Aperture)

function [Tx_aperture, Rx_aperture] = simulate_rf_line(line_no, probe_angle, opts, amplitudes, positions, y_slice);
    
    % Status
    disp(['Now making line ',num2str(line_no)])

    % =============================================================================================================================================
    % Probe and simulation parameters for FieldII
    % =============================================================================================================================================
    
    f0 = opts.f0;                                   %  Transducer center frequency [Hz]
    fs = opts.fs;                                   %  Sampling frequency [Hz]
    c = opts.c;                                     %  Speed of sound [m/s]
    N_active = opts.N_active;                       %  Number of active elements
    z_focus = opts.xmitfocus + opts.z_start;        %  Transmit focus
    apo = hanning(N_active)';                       %  Set the apodization for reception
    no_lines = opts.no_lines;                       %  Number of lines in image
    image_width = opts.x_size;                      %  Size of image sector
    d_x = image_width/no_lines;                     %  Increment for image
    % =============================================================================================================================================

    % Initialize FieldII program
    field_init(-1);

    % =============================================================================================================================================
    % Create probe in FieldII
    % =============================================================================================================================================

    % set the sampling frequency the system uses
    set_sampling(fs);
    % Generate aperture for emission , create a linear array transducer.
    % Tx_aperture = xdc_linear_array(N_elements, width, height, kerf, no_sub_x, no_sub_y, focus);
    % no_sub_x:     number of sub-divisions in x-direction of elements
    % no_sub_y:     number of sub-divisions in y-direction of elements
    Tx_aperture = xdc_linear_array(opts.N_elements, opts.width, opts.element_height, opts.kerf, 1, 10, opts.focus);
    % set the impulse response for the transmit aperture
    % transmit apodization -> hanning
    impulse_response = sin(2*pi*f0*(0:1/fs:2/f0));
    impulse_response = impulse_response.*hanning(max(size(impulse_response)))';
    % procedure for setting the impulse response for the transmit aperture
    xdc_impulse(Tx_aperture, impulse_response);
    % set the impulse response and excitation for the transmit aperture
    excitation = sin(2*pi*f0*(0:1/fs:2/f0));
    % procedure for setting the excitation pulse for the transmit aperture
    xdc_excitation(Tx_aperture, excitation);
    % generate aperture for reception
    Rx_aperture = xdc_linear_array(opts.N_elements, opts.width, opts.element_height, opts.kerf, 1, 10, opts.focus);
    % set the impulse response for the receive aperture
    xdc_impulse(Rx_aperture, impulse_response);

    % The imaging direction
    x = -image_width/2 + (line_no-1)*d_x;
    
    % set the focus for this direction with the proper reference point
    % for dynamic focusing
    xdc_center_focus(Tx_aperture, [x 0 0]);
    xdc_focus(Tx_aperture, 0, [x 0 z_focus]);
    xdc_center_focus(Rx_aperture, [x 0 0]);
    xdc_dynamic_focus(Rx_aperture, opts.z_start / c, 0, 0);
    
    % calculate the apodization
    N_pre  = round(x/(opts.width+opts.kerf) + opts.N_elements/2 - N_active/2);
    N_post = opts.N_elements - N_pre - N_active;
    apo_vector=[zeros(1,N_pre) apo zeros(1,N_post)];
    
    % procedure for creating an apodization time line for an transmit aperture
    xdc_apodization(Tx_aperture, 0, apo_vector);
    
    % procedure for creating an apodization time line for an receive aperture
    xdc_apodization(Rx_aperture, 0, apo_vector);
    % =============================================================================================================================================



    % =============================================================================================================================================
    % Simulate a single RF line and save the simulation result 
    % =============================================================================================================================================

    % Test if the file for the line exists skip the simulation, if the line exits and go the next line. Else make the simulation
    
    %rf_file_name = [opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/rf_pos_',num2str(y_slice),'_ln_',num2str(line_no),'.mat'];
    %if ~exist(rf_file_name,'file')
            
    % Simulate Ultrasound by calculating received signal though the probe we designed
    [rf_data, tstart] = calc_scat(Tx_aperture, Rx_aperture, positions, amplitudes);

    % TODO: Replace the eval-based syntax
    % Store the received signal
    cmd=['save ',opts.rfpath,'/',num2str(probe_angle*360/(2*pi)),'/rf_pos_',num2str(y_slice),'_ln_',num2str(line_no),'.mat rf_data tstart'];
    eval(cmd);
         
    %end 
    % =============================================================================================================================================
    

    % free space for apertures
    xdc_free (Tx_aperture)
    xdc_free (Rx_aperture)

end % End function simulate_rf_line