% Orcun Goksel (c) Aug 2012
function [hdr img] = read_b8(filename)
file = fopen(filename,'rb');
hdr = ultrasonixReadHeader(file);
if nargout==1
    fclose(file);
    return
end

fseek(file,hdr.file.headerSizeBytes,-1);
if hdr.ss==8, imgtype='*uint8'; else imgtype='*int16'; end
img = fread(file,inf,imgtype);
fclose(file);
if hdr.filetype == 512
    img = reshape(img, hdr.h, hdr.extra, hdr.w, []);
    img = squeeze(mean(img,2));
elseif hdr.filetype == 16
    img = reshape(img, hdr.h, hdr.w, []);
else
    img = reshape(img, hdr.w, hdr.h, []);
end

%img = imrotate(img, -90);

% Remove margins (are those few-pixel black margins constant among acquisitions?)
%img = img(hdr.ul(2)+1:hdr.br(2)-1,hdr.ul(1)+2:hdr.br(1)-2,:);

% tmp(:,:,1,:) = img;
% implay(tmp,40);  % FPS should be hdr.dr, but 84 fps is too high for Matlab


% from uread.m by Paul Otto (potto@gmu.edu), based on code by corina.leung@ultrasonix.com
function [header]=ultrasonixReadHeader(fid)

fPass=fseek(fid,0,-1);
if fPass~=0
    error('fseek failed')
end

% read the header info
hinfo = fread(fid, 19, 'int32');

% load the header information into a structure and save under a separate file
header = struct('filetype', 0, 'nframes', 0, 'w', 0, 'h', 0, 'ss', 0, 'ul', [0,0], 'ur', [0,0], 'br', [0,0], 'bl', [0,0], 'probe',0, 'txf', 0, 'sf', 0, 'dr', 0, 'ld', 0, 'extra', 0);
header.filetype = hinfo(1);
header.nframes = hinfo(2);
header.w = hinfo(3);
header.h = hinfo(4);
header.ss = hinfo(5);
header.ul = [hinfo(6), hinfo(7)];
header.ur = [hinfo(8), hinfo(9)];
header.br = [hinfo(10), hinfo(11)];
header.bl = [hinfo(12), hinfo(13)];
header.probe = hinfo(14);
header.txf = hinfo(15);
header.sf = hinfo(16);
header.dr = hinfo(17);
header.ld = hinfo(18);
header.extra = hinfo(19);

%frameSizeBytes=(header.w*header.h)*2+(1*4);
if mod(header.ss,8)~=0
    error(['Unsupported sample size of ' num2str(header.ss) ' bits when reading the header.  Sample size must be a multiple of 8 bits.']);
end

%we need to decide what file version this is:
%Version Name | Description
% 1.0         | This is the "original" version we used with frame tag
%             | numbers (4-bytes - long int).   SonixRP 3.2.2 uses it.
% 2.0         | This version  which is used with Sonix RP 5.6.5 does not
%             | have frame tag numbers.

headerSizeBytes=(19*4);
frameSizeBytesWithoutTag=(header.w*header.h)*(header.ss/8);
% For reading doppler images, multiple by number of ensembles.
% Added by Orcun for reading crf/cvv files, but I'm not sure if it always works
if header.extra, frameSizeBytesWithoutTag=frameSizeBytesWithoutTag*header.extra; end

%To do the check we look at the file size and compare the total size
fPass=fseek(fid,0,'eof');
if fPass~=0
    error('fseek failed')
end
fileSizeInBytes=ftell(fid);

header.file.headerSizeBytes=headerSizeBytes;
if fileSizeInBytes == (headerSizeBytes+(frameSizeBytesWithoutTag+4)*header.nframes)
    header.file.version='1.0';
    header.file.frameSizeBytes=frameSizeBytesWithoutTag+4;
elseif fileSizeInBytes == (headerSizeBytes+frameSizeBytesWithoutTag*header.nframes)
    header.file.version='2.0';
    header.file.frameSizeBytes=frameSizeBytesWithoutTag;
else
    error('UREAD:UNSUPPORTED_VERSION',['Unsupported file type.  There are ' num2str(fileSizeInBytes-(headerSizeBytes+frameSizeBytesWithoutTag*header.nframes)) ' unexpected bytes.']);
end