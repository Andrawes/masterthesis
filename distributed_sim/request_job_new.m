% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY : 
% This function sends a request to JobAllocator for a new simulation job.
% It parses the answer from JobAllocator to extract the details about the
% simulation job to be run, and stores this configuration in a struct 
% called "job".
%
% INPUT : 
%   res_path :  (string) Directory where the message files to 
%               Job Allocator are written
%   thread_id:  (string) ID of the current thread
% OUTPUT:
%   job:        job struct containg a description of the allocated simulation job.

function job = request_job_new(res_path, thread_id)
    
    %==================================================================================
    % Defining key strings and variables used in communications with JobAllocator
    %==================================================================================
    % Job Request String to request a job reservation from JobAllocator
    job_request = strcat(res_path,'/request_',thread_id,'.mat');

    % Regular expression selecting all the granted jobs for this thread
    all_granted_re = strcat(res_path,'/granted_',thread_id,'*');
    all_granted_re = convertStringsToChars(all_granted_re);

    % Other variables
    dummy_str = '';             % Empty string
    %==================================================================================

    

    %==================================================================================
    % Defining key strings used in communications with JobAllocator
    %==================================================================================

    % Search among all "granted" signals for this thread
    all_granted_files = dir(all_granted_re)';

    % While no job granted message is found
    while length(all_granted_files) == 0   
        % Send request (reservation) for a job
        save(job_request, 'dummy_str');
        % Pause execution for 1 second (to allow for JobAllocator to process request)       
        pause(1);     
        % Search again for corresponding acknowledgement files 
        all_granted_files = dir(all_granted_re)';
    end

    %Search among all "granted" signals 
    for file = all_granted_files
        
        % Remove extension ".mat" from file
        job_data = split(file.name,'.');
        
        % Parse the granted signal
        job_data = split(job_data(1),'_');
        job.action = string(job_data(1));
        job.thread_id = thread_id;
        job.type = string(job_data(3));
        job.exp_name = string(job_data(4));
        job.rf_line = convertStringsToChars(string(job_data(5)));
        job.depth = convertStringsToChars(string(job_data(6)));
        job.ang = convertStringsToChars(string(job_data(7)));
    end
    %==================================================================================
end