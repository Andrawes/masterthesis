% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY : 
% This function takes a 3D scatter map, and rotates or translates it to
% to simulate the positioning of the US probe relative to the scatter map.
% It then launches the simulation of the RF line specified by JobAllocator. 
%
% INPUT : 
%   opts:           Options structure for this simulation
%   probe_angle:    Angle in radians of US beam incidence
%   rf_line:        The number of the RF-line to be simulated 
%   y_slice:        Depth of the current slice when simulating a 3D volume
%   amplitudes:     Vector of amplitudes of the scatterers in the scatter map
%   x:              Vector of x-coordinates of the scatterers
%   y:              Vector of y-coordinated of the scatterers
%   z:              Vector of z_coordinated of the scatterers
%

function [] = sim_img(opts, rf_line, probe_angle, y_slice, x, y, z, amplitudes)

    % ==================================================================================================
    % Now we prune all the scatterers which are not in the current slice and translate the remaining 
    % scatterers such that the slice center is at y=0. This simulates the motion of the probe in the 
    % y-direction. The reason we perform this pruning is that scatteres too far away would otherwise 
    % contribute to the simulation time, but would be barely visible in the final B-mode image.
    % ==================================================================================================

    % Translate the scatterers such that the probe is above the center of the slice
    y = y-opts.y_slice_center(y_slice);
    
    % Prune the scatterers not in the slice by making a mask
    prune_mask = (y>(-opts.y_slice_depth/2)) & (y<opts.y_slice_depth/2);
    
    x = x(prune_mask);
    y = y(prune_mask);
    z = z(prune_mask);
    amplitudes = amplitudes(prune_mask);
    
    display(strcat('Slice pruning: Removed ',num2str((1-(numel(amplitudes)/numel(prune_mask)))*100),"%"))
    % ==================================================================================================
    
    
    
    % ==================================================================================================
    % Now we rotate the position of the scatterers (to emulate the pivoting of the probe around the ROI)
    % ==================================================================================================
    
    % Center of rotation ([mm],[mm])
    r_c = opts.r_c;
    r_c_x = r_c(1,:);
    r_c_y = r_c(2,:);
    r_c_z = r_c(3,:);
    
    % Radius in [mm] of the inscribed circle
    rad = sqrt((opts.x_size/opts.x_scale_factor)^2+(opts.z_size/opts.z_scale_factor)^2)/2 ;
    
    % Rotation matrix (around the Y_axis)
    R_phi = [cos(probe_angle), -sin(probe_angle); sin(probe_angle), cos(probe_angle)];
    
    % Translate such that the center of rotation is at (0,0)
    % Then rotate all the speckles, then translate back
    rotated_pos = R_phi*[(x-r_c_x)';(z-r_c_z)'] ;
    rotated_pos = rotated_pos+[r_c_x;r_c_z];
    
    % Concatenate x,y,z coordinates into a single matrix
    positions = [rotated_pos(1,:)' y rotated_pos(2,:)'];
    % ==================================================================================================
    


    % ==================================================================================================
    % Now, we render the RF lines one-by-one
    % ==================================================================================================
    
    % Simulate a single rf_line
    [Tx_aperture, Rx_aperture] = simulate_rf_line(rf_line, probe_angle, opts, amplitudes, positions, y_slice);

    % ==================================================================================================

end % End function sim_img

