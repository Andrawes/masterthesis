% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY : 
% This function reads the type of task which has been allocated (t1, t2, or t3), and acts
% accordingly.
% t1: Generation of tissue phantoms
% t2: Simulation of US interaction with tissue phantoms (scatter maps)
% t3: US b-mode image creation from the simulated data 
%
% INPUT : 
%   simulate:           Flag. Used to decide whether to simulate images or not. Used in 
%                       debugging to see the result of scatter map generation without 
%                       simulating its interaction with US. 
%   fast_run:           Flag. Used in debugging for running the US simulation quickly
%   evaluation_mode:    Flag. If true, means the program is in evaluation mode, else
%                       in dataset generation mode. 
%   sim2d:              Flag. If true, indicates the program is a 2D simulation (not 3D)
%   new_scatmap:        Flag. If true, indicates that a new random scatter-map can be generated.
%   render_images:      Flag. If true, indicates whether to render the images or not.
%   job:                job struct containg a description of the allocated simulation job.
%   override_angles:    Flag. If true, indicates that the simulation angle is given in experiment name
%   generate_incl:      Flag. This is a special mode. When set to true, the program generates a 
%                       scatter map of a circular inclusion 
%   generate_from_ct:   Flag. Whether to use CT images, or the method of random ellipsoids for 
%                       scatter map generation.


function [] = simulation_routine(simulate, fast_run, evaluation_mode, sim2d, new_scatmap, render_images, job, override_angles, generate_incl, generate_from_ct)	

    % Set the name of the current experiment
    opts = optsInit(job.exp_name, sim2d, override_angles);
    
    if job.type == 't1'
        % ================================================================================================
        % Scatter map generation job (type 1)
        % ================================================================================================

        % Create scatter map (phantom) (returns coordinates of scatterers and their amplitudes)
        if generate_incl == false
            [x, y, z , amp] = make_phantom(opts, fast_run, evaluation_mode, sim2d, new_scatmap, generate_from_ct);
        else
            [x, y, z , amp] = make_cyst(opts, fast_run, evaluation_mode, sim2d, new_scatmap);
        end
        % ================================================================================================
    
    elseif job.type == 't2'
        % ================================================================================================
        % RF line simulation job (type 2)
        % ================================================================================================

        % Load scatter map data
        file_name = [opts.scatter_map,'/scatterer_data.mat'];
        load(file_name); % loads variables "x_s", "y_s", "z_s" and "amplitudes"
        %x = x_s; y = y_s; z = z_s; amp = amplitudes;

        % Simulate rf-line
        if override_angles==true
            sim_img(opts, str2num(job.rf_line), str2num(opts.forced_angle)*pi/180, str2num(job.depth), x_s, y_s, z_s, amplitudes);
        else
            sim_img(opts, str2num(job.rf_line), str2num(job.ang)*pi/180, str2num(job.depth), x_s, y_s, z_s, amplitudes);
        end
        
        % ================================================================================================

    elseif job.type == 't3'
        % ================================================================================================
        % Render image job (type 3)
        % ================================================================================================
        
        % Make B-mode image 
        if override_angles == true
            % Wether to render bmode image (true) or RF image (false)
            render_bmode = true;
            make_image(opts, str2num(opts.forced_angle)*pi/180, str2num(job.depth), render_bmode);
            % Make RF image
            render_bmode = false;
            make_image(opts, str2num(opts.forced_angle)*pi/180, str2num(job.depth), render_bmode);  
        else
            % Wether to render bmode image (true) or RF image (false)
            render_bmode = true;
            make_image(opts, str2num(job.ang)*pi/180, str2num(job.depth), render_bmode);
            % Make RF image
            render_bmode = false;
            make_image(opts, str2num(job.ang)*pi/180, str2num(job.depth), render_bmode);  
        end
        
        % ================================================================================================
    else 
        display('Incorrect Job Type')
    end
    
end % End function simulation routine
