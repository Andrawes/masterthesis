#$ -t 1-999
#$ -o /home/aalbahou_msc/data/logs
#$ -N testLog
#$ -l h_vmem=25G
#$ -l h_rt=00:45:00
#$ -cwd
#$ -j y

SGE_TASK_LAST=${SGE_TASK_LAST:-1}
#SGE_TASK_ID=${SGE_TASK_ID:-1}

TASK_ID=${SGE_TASK_ID:-"$1"}

TEMPDIR=/scratch/${RANDOM}_${RANDOM} 

mkdir $TEMPDIR
cd /scratch_net/biwidl103/aalbahou/masterthesis/cluster_scripts
echo $SGE_TASK_ID > log.log
export MCR_CACHE_ROOT=$TEMPDIR

#EXECUTION_STRING=main\(\"$TASK_ID\"\)
EXECUTION_STRING=main\(\"$TASK_ID\"\)

cd /scratch_net/biwidl103/aalbahou/masterthesis/distributed_sim
#exec /usr/sepp/bin/matlab -nodisplay -singleCompThread -nodesktop -nosplash -r "main(`$TASK_ID`)" > log.log
exec /usr/sepp/bin/matlab -nodisplay -nodesktop -nosplash -r  "`echo $EXECUTION_STRING`" > log.log

#echo "Hello World" > /srv/glusterfs/aalbahou/common_dir/something.txt 
#./run_main.sh /usr/pack/matlab-2017b-fg

rm -rf $TEMPDIR

