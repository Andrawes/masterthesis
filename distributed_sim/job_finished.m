% Andrawes Al Bahou (c) - ETH Zurich - 2018

% FUNCTIONALITY:
% This function is used to interact with the JobAllocator. 
% At the end of a certain job, the thread can use this function
% to signal to JobAllocator that the task has been accomplished.
% *Note: Due to a problem with the API, this method is currently
% implemented by writing an empty file, whose name is the signal 
% to be sent to JobAllocator (JobAllocator then scans for these 
% files.)

% INPUT: 
%	res_path : 	(string) Directory where the message files to 
%				Job Allocator are written
%	thread_id: 	(string) ID of the current thread

function [] = job_finished (res_path, thread_id)

    % "Job finished" signal to be sent to JobAllocator
    job_finished = strcat(res_path,'/remove_',thread_id,'.mat');   
    
    % Empty string
    dummy_str = '';

    % Send a reservation-removal-request to the reservation manager 
    save(job_finished,'dummy_str');

    pause(1)
end
