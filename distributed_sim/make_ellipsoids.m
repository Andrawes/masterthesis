% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function takes a regular 3D grid of points, and assigns them
% amplitudes. It does this by creating randomly-sized and rotated 
% ellipsoids in that 3D space. The distance from the center of these
% ellipsoids describes a random spatial gradient. This spatial 
% gradient is then used to assign amplitudes to the points in space.
%
% INPUT: 
%   opts:       Options structure for this simulation.
%   x_size:     Lateral size of the phantom
%   y_size:     Elevation size of the phantom
%   z_size:     Axial size of the phantom 
%   z_start:    Distance of the phantom from the transducer
%   x_g:        Vector of x_coordinates of the points of the input grid
%   y_g:        Vector of y_coordinates of the points of the input grid
%   z_g:        Vector of z_coordinates of the points of the input grid
%
% OUTPUT: 
%   multiplicative_amplitude :  Vector of amplitudes assigned to the points
%                               of the input 3D grid

function multiplicative_amplitude = make_ellipsoids(opts, x_size, y_size, z_size, z_start, x_g, y_g, z_g)

    % Number of ellipsoids to insert
    n_ellipsoids = 10;
    % Initialize matrix of amplitudes assigned to the interpolation grid
    multiplicative_amplitude = ones(1,numel(x_g));

    for n_ell = 1:n_ellipsoids

        xc = (rand()*(x_size/opts.x_scale_factor)/2)-(x_size/opts.x_scale_factor)/4;            % Center of ellipse (x-coordinate)
        yc = (rand()*(y_size)/2)-(y_size)/4;                                                    % Center of ellipse (y-coordinate)
        zc = (rand()*(z_size/opts.z_scale_factor)/2)+z_start+(z_size/opts.z_scale_factor)/4;    % Center of ellipse (z-coordinate)

        % Center of 3D volume
        r_c = opts.r_c;
        r_c_x = r_c(1,:);
        r_c_y = r_c(2,:);
        r_c_z = r_c(3,:);

        h = rand()*(x_size/opts.x_scale_factor)/4;  % Horizontal major radius
        v = rand()*(z_size/opts.z_scale_factor)/4;  % vertical major radius
        a = rand()*y_size/4;                        % Axial major radius
        
        phi_x = pi*rand();                          % Rotation of the ellipse (x-axis)
        phi_y = pi*rand();                          % Rotation of the ellipse (y-axis)
        phi_z = pi*rand();                          % Rotation of the ellipse (z-axis)

        % Rotation matrices
        Rx = [1, 0, 0;
              0, cos(-phi_x), -sin(-phi_x);
              0, sin(-phi_x), cos(-phi_x)];
        Ry = [cos(-phi_y), 0 , sin(-phi_y);
              0,1,0;
              -sin(-phi_y), 0, cos(-phi_y)];
        Rz = [cos(-phi_z), -sin(-phi_z), 0;
              sin(-phi_z), cos(-phi_z), 0;
              0,0,1];

        % Scaling matrix
        S = [1,0,0;
             0,h/a,0;
             0,0,h/v];

        % Flatten the coordinate matrices
        x_g_f = reshape(x_g, 1,[]);
        y_g_f = reshape(y_g, 1,[]);
        z_g_f = reshape(z_g, 1,[]);

        % Rotate the scatterer space about the center point of the scatterer space (Such that the orientation of the ellipsoid is correct relative to the space)
        grid_coordinates_new = (Rx*Ry*Rz*[x_g_f-r_c_x; y_g_f-r_c_y; z_g_f-r_c_z])+r_c;

        % Translate the scatterer space (such as the ellipsoid is centered at its intended center)
        grid_coordinates_new = grid_coordinates_new-[xc;yc;zc];

        % Transform the scatterer space (such that the ellipsoid becomes a sphere in this space)
        grid_coordinates_new = S*grid_coordinates_new;
 
        % Assign ellipse types randomly
        ellipse_type = 1+floor(rand()*2);
        % Assign a random falloff gradient from the center of the ellipsoid
        amplitude_gradient = 1+(rand());
        
        % Type-1 ellipsoids are bright in the center and get darker, and vice-versa for type-2 ellipsoids
        if ellipse_type == 1
            if (a*h*v < 1e-6) % If the volume of the ellipsoid is small, make the falloff steeper
                amplitude_gradient = amplitude_gradient*5;
            end
            % The distance of the point from the center of the ellipsoid (which in the new space is now a sphere)
            multiplicative_amplitude =  multiplicative_amplitude.*(1.2-(((sum(grid_coordinates_new.^2)).^(1/2))*amplitude_gradient));
        else                
            % The distance of the point from the center of the ellipsoid (which in the new space is now a sphere)
            multiplicative_amplitude =  multiplicative_amplitude.*(1+(((sum(grid_coordinates_new.^2)).^(1/2))*amplitude_gradient));
        end

    end % End for loop
end % End function make_ellipsoids