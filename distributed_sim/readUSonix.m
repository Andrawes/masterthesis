% Copyright (C) 2016 ETH Zurich - All Rights Reserved.
% Written by Oliver Mattausch, Computer Vision Lab, ETHZ D-ITET.

% reads a .vol file from Ultrasonix
function [X, volumeAngle, lineDensity, frameAngle, depth, height] = readUSonix(file)
    volumeAngle = 0;
    lineDensity = 0;
    frameAngle = 0;
    depth = 0;

    [path filename ext] = fileparts(file);
    if strcmp(ext,'.vol')
        % open file
        fileID = fopen(file,'r');
        % read information from header
        type = fread(fileID,1,'int32'); % 0=prescan, 1=postscan, 2=rf
        noVolumes = fread(fileID,1,'int32');
        framesVolume = fread(fileID,1,'int32');
        width = fread(fileID,1,'int32');
        height = fread(fileID,1,'int32');
        bits = fread(fileID,1,'int32');
        deltaDegree = fread(fileID,1,'int32');
        volumeAngle = deltaDegree*(framesVolume-1);

        if(type==0)
            tmp = width;
            width = height;
            height = tmp;
        end
        X(:,:,:,:) = zeros(height,width,framesVolume,noVolumes);

        if (bits==8)
            for a=1:noVolumes
                % read frames from file
                for z=1:framesVolume
                    tmp = fread(fileID,[width height],'uint8');
                    X(:,:,z,a) = tmp';
                end
            end
        end
        % close file
        fclose(fileID);
    elseif strcmp(ext,'.3dd')
        import javax.xml.xpath.*
        factory = XPathFactory.newInstance;
        xpath = factory.newXPath;

        % read parameters from xml file
        xDoc = xmlread([file '.xml']);
        expression = xpath.compile('Params/FourD/nimages_per_volume');
        node = expression.evaluate(xDoc, XPathConstants.NODE);
        frames = str2num(node.getTextContent);
        expression = xpath.compile('Params/FourD/sampleNum');
        node = expression.evaluate(xDoc, XPathConstants.NODE);
        width = str2num(node.getTextContent);
        %expression = xpath.compile('Params/FourD/lineNum');
        %node = expression.evaluate(xDoc, XPathConstants.NODE);
        %height = node.getTextContent;
        expression = xpath.compile('Params/FourD/volume_angle');
        node = expression.evaluate(xDoc, XPathConstants.NODE);
        volumeAngle = str2num(node.getTextContent); % in �
        expression = xpath.compile('Params/FourD/sampleDistance_mm');
        node = expression.evaluate(xDoc, XPathConstants.NODE);
        depth = str2num(node.getTextContent); % in mm
        expression = xpath.compile('Params/FourD/frame_angle');
        node = expression.evaluate(xDoc, XPathConstants.NODE);
        frameAngle = str2num(node.getTextContent); % in �
        expression = xpath.compile('Params/FourD/lineDensity');
        node = expression.evaluate(xDoc, XPathConstants.NODE);
        lineDensity = str2num(node.getTextContent);
        
        fileInfo = dir(file);
        fileSize = fileInfo.bytes;
        height = fileSize/frames/width; % should be 'lineNum' but is not in the current data!!!
        % open file
        fileID = fopen(file,'r');

        X = zeros(height,width,frames);
        for z=1:frames
            tmp = fread(fileID,[width height],'uint8');
            X(:,:,z) = tmp';
        end
    elseif strcmp(ext,'.b8')
        % open file
        fileID = fopen(file,'r');
        % read information from header
        type     = fread(fileID,1,'int32') % 0=prescan, 1=postscan, 2=rf
        noFrames = fread(fileID,1,'int32')
        width    = fread(fileID,1,'int32')
        height   = fread(fileID,1,'int32')
        bits     = fread(fileID,1,'int32')
        x_ul = fread(fileID,1,'int32'); % upper left x
        y_ul = fread(fileID,1,'int32'); % upper left y
        x_ur = fread(fileID,1,'int32'); % upper right x
        y_ur = fread(fileID,1,'int32'); % upper right y
        x_lr = fread(fileID,1,'int32'); % lower right x
        y_lr = fread(fileID,1,'int32'); % lower right y
        x_ll = fread(fileID,1,'int32'); % lower left x
        y_ll = fread(fileID,1,'int32'); % lower left y
        probe       = fread(fileID,1,'int32');
        transFreq   = fread(fileID,1,'int32'); % in Hz
        sampFreq    = fread(fileID,1,'int32'); % in Hz
        dataRate    = fread(fileID,1,'int32'); % in frames per second
        lineDensity = fread(fileID,1,'int32');
        extra       = fread(fileID,1,'int32');

        % hack matt: only need one frame
        X(:,:) = zeros(height,width);
        % read frames from file   
        tmp = fread(fileID,[width height],'uint8');
        X(:,:) = tmp';
    elseif strcmp(ext,'.b32')
        % open file
        fileID = fopen(file,'r');
        % read information from header
        type     = fread(fileID,1,'int32') % 0=prescan, 1=postscan, 2=rf
        noFrames = fread(fileID,1,'int32')
        width    = fread(fileID,1,'int32')
        height   = fread(fileID,1,'int32')
        bits     = fread(fileID,1,'int32')
        x_ul = fread(fileID,1,'int32'); % upper left x
        y_ul = fread(fileID,1,'int32'); % upper left y
        x_ur = fread(fileID,1,'int32'); % upper right x
        y_ur = fread(fileID,1,'int32'); % upper right y
        x_lr = fread(fileID,1,'int32'); % lower right x
        y_lr = fread(fileID,1,'int32'); % lower right y
        x_ll = fread(fileID,1,'int32'); % lower left x
        y_ll = fread(fileID,1,'int32'); % lower left y
        probe       = fread(fileID,1,'int32');
        transFreq   = fread(fileID,1,'int32'); % in Hz
        sampFreq    = fread(fileID,1,'int32'); % in Hz
        dataRate    = fread(fileID,1,'int32'); % in frames per second
        lineDensity = fread(fileID,1,'int32');
        extra       = fread(fileID,1,'int32');

        % hack matt: only need one frame
        X(:,:) = zeros(height,width);
        if (bits == 32)
            % read frames from file   
            tmp = fread(fileID,[width height],'uint32');
            X(:,:) = tmp';
        end
    elseif strcmp(ext,'.bpr')
        % open file
        fileID = fopen(file,'r');
        % read information from header
        type     = fread(fileID,1,'int32') % 0=prescan, 1=postscan, 2=rf
        noFrames = fread(fileID,1,'int32')
        width    = fread(fileID,1,'int32')
        height   = fread(fileID,1,'int32')
        bits     = fread(fileID,1,'int32')
        x_ul     = fread(fileID,1,'int32'); % upper left x
        y_ul     = fread(fileID,1,'int32'); % upper left y
        x_ur     = fread(fileID,1,'int32'); % upper right x
        y_ur     = fread(fileID,1,'int32'); % upper right y
        x_lr     = fread(fileID,1,'int32'); % lower right x
        y_lr     = fread(fileID,1,'int32'); % lower right y
        x_ll     = fread(fileID,1,'int32'); % lower left x
        y_ll     = fread(fileID,1,'int32'); % lower left y
        probe    = fread(fileID,1,'int32');
        transFreq   = fread(fileID,1,'int32') % in Hz
        sampFreq    = fread(fileID,1,'int32') % in Hz
        dataRate    = fread(fileID,1,'int32') % in frames per second
        lineDensity = fread(fileID,1,'int32')
        extra       = fread(fileID,1,'int32')
        %dum = fread(fileID, 1, 'int32') % 4 byte frame header
        
        % hack matt: only need one frame
        %X(:,:) = zeros(height, width);
        %if (bits == 8)
        %    % read frames from file   
        %    tmp = fread(fileID, [height width], 'uint8'); X(:,:) = tmp;
        %end
         X(:,:,:) = zeros(height,width,noFrames);
        if (bits == 8)
            % read frames from file
            for z=1:noFrames
                tmp = fread(fileID, [height width],'uint8');
                X(:,:,z) = tmp;
            end
        end
    elseif strcmp(ext,'.rf')
        % open file
        fileID = fopen(file,'r');
        % read information from header
        type        = fread(fileID,1,'int32') % 0=prescan, 1=postscan, 2=rf
        noFrames    = fread(fileID,1,'int32')
        width       = fread(fileID,1,'int32')
        height      = fread(fileID,1,'int32')
        bits        = fread(fileID,1,'int32')
        x_ul        = fread(fileID,1,'int32'); % upper left x
        y_ul        = fread(fileID,1,'int32'); % upper left y
        x_ur        = fread(fileID,1,'int32'); % upper right x
        y_ur        = fread(fileID,1,'int32'); % upper right y
        x_lr        = fread(fileID,1,'int32'); % lower right x
        y_lr        = fread(fileID,1,'int32'); % lower right y
        x_ll        = fread(fileID,1,'int32'); % lower left x
        y_ll        = fread(fileID,1,'int32'); % lower left y
        probe       = fread(fileID,1,'int32');
        transFreq   = fread(fileID,1,'int32') % in Hz
        sampFreq    = fread(fileID,1,'int32') % in Hz
        dataRate    = fread(fileID,1,'int32') % in frames per second
        lineDensity = fread(fileID,1,'int32')
        extra       = fread(fileID,1,'int32')

        % hack matt: only need one frame
        %X(:,:) = zeros(height, width);
        %if (bits == 8)
            % read frames from file   
        %   tmp = fread(fileID, [height width], 'int16');
        %   X(:,:) = tmp;
        %end
        X(:,:,:) = zeros(height,width,noFrames);
        % read frames from file
        for z=1:noFrames
            tmp = fread(fileID, [height width],'int16');
            X(:,:,z) = tmp;
        end
    end
end