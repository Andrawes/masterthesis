% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function is where all the simulation settings, probe parameters, 
% files, directories, and various other options for the FieldII simulator, 
% and the phantom generation procedure are set.
%
% INPUT: 
%   name:               (String). Name of the current experiment. An experiment 
%                       is defined as all the different simulation settings of 
%                       one particular phantom.
%   sim2d:              Flag. If true, indicates the program is a 2D simulation (not 3D)
%   override_angles:    Flag. If true, indicates that the simulation angle is given in experiment name
%
% OUTPUT:
%   opts:               Options structure for this simulation

function opts = optsInit(name, sim2d, override_angles)

    opts.experiment_name = name;            % Experiment name
    
    % =======================================================================================================
    % Phanton parameters and probe rotation settings
    % =======================================================================================================
    opts.x_size = 40/1000;                  %  Width of phantom [mm]
    opts.z_size = 50/1000;                  %  Height of phantom [mm]
    opts.z_start = 5/1000;                  %  Start of phantom surface [mm];
    opts.y_slice_depth = 2*0.0027;          %  Depth of each 3D slab/slice

    if sim2d== true
        opts.y_size = opts.y_slice_depth;   %  Transverse width of phantom [mm]
    else
        opts.y_size = 15/1000;              %  Transverse width of phantom [mm]    
    end

    % resize to cover for blind spots during probe rotation
    opts.x_scale_factor = sqrt(opts.x_size^2+opts.z_size^2)/opts.x_size;
    opts.z_scale_factor = sqrt(opts.x_size^2+opts.z_size^2)/opts.z_size;
    opts.x_size = sqrt(opts.x_size^2+opts.z_size^2);
    opts.z_size = opts.x_size;
    
    % Z-dimension offset due to inscribed region of interest in circle
    opts.circ_offset_z = opts.z_size*(1-1/opts.z_scale_factor)/2 ;
    
    % Center of rotation ([mm],[mm]) describes the point around which the US 
    % probe rotates
    opts.r_c_x = 0.0; % Center of rotation coordinates (x-axis)
    opts.r_c_y = 0.0; % Center of rotation coordinates (y-axis)
    opts.r_c_z = opts.z_start+opts.z_size/2; % Center of rotation coordinates (z-axis)
    opts.r_c = ([opts.r_c_x, opts.r_c_x, opts.r_c_z])'; 

    % Speckle density and number of speckles
    opts.N_mm3 = 20;
    opts.N = (opts.x_size*1e3) * (opts.y_size*1e3) * (opts.z_size*1e3) * opts.N_mm3;

    % If the experiment forces the use of a given angle, then this variable would contain its value
    opts.forced_angle = '';
    % List of angles at which the ROI is imaged (pivot of the US probe) [radians]
    if override_angles == true
        % Extract angle information from the experiment name
        ang_info = regexp(opts.experiment_name, '\^', 'split');
        opts.forced_angle = char(ang_info(2));
        opts.probe_angles = str2num(opts.forced_angle)*2*pi/360;
    else
        opts.probe_angles = (-1)*[0:1:0]*2*pi/360;
    end
    % =======================================================================================================



    % =======================================================================================================
    % 3D vs 2D simulation settings
    % =======================================================================================================
    if sim2d==true
        opts.n_y_slices = 1;
        opts.y_slice_center = [0];
    else
        opts.n_y_slices = 3;
        opts.y_slice_center = [-opts.y_slice_depth, 0 ,opts.y_slice_depth];
    end
    % =======================================================================================================
    


    % =======================================================================================================
    % FieldII US probe parameters
    % =======================================================================================================

    opts.f0_mult_fact = 2;                              % Multiplicative factor by which to multiply certain 
                                                        % quantities in order to adjust image to new sampling
                                                        % frequency. Base frequency is 5Mhz, and is multiplied
                                                        % by this multiplicative factor.
    opts.f0 = 5e6*opts.f0_mult_fact;                    % Transducer center frequency [Hz]
    opts.fs = 40e6;                                     % Sampling frequency [Hz]
    opts.c = 1540;                                      % Speed of sound [m/s]
    opts.lambda = opts.c/opts.f0;                       % Wavelength [m]
    opts.element_height = 3/1000;                       % Height of element [m]
    opts.kerf = 0.0045/1000/opts.f0_mult_fact;          % Kerf [m] (distance in x-direction between elements)
    opts.width = opts.lambda;                           % Width of element
    opts.no_lines = round(128*opts.x_scale_factor*opts.f0_mult_fact);     % Number of RF lines
    opts.N_active = round(64*opts.x_scale_factor);      % Number of active elements
    %opts.N_elements = round(192*opts.x_scale_factor);   % Number of physical elements
    opts.N_elements = opts.no_lines + opts.N_active;    % Number of physical elements
    opts.focus = [0 0 40]/1000;                         % Fixed focal point [m]
    opts.xmitfocus = 25/1000;                           % Transmit focus
    opts.f_number = 1.5;                                % f-number (focal depth / aperture)

    %opts.no_IDs = opts.no_lines*opts.n_y_slices*length(opts.probe_angles);
    % =======================================================================================================



    % =======================================================================================================
    % US image processing options
    % =======================================================================================================
    % Clip to -40dB all parts of the bmode signal with amplitude less than -40dB 
    opts.db40 = true;
    
    % apodization plotting option
    opts.excSignalPlot = false;
    opts.apodPlot = false;
    % =======================================================================================================



    % =======================================================================================================
    % Paths for inputs and outputs
    % =======================================================================================================
    if (~isdeployed) % To distinguish between my cluster and the biwi cluster
        %opts.path = '/srv/glusterfs/aalbahou/common_dir';
        opts.path = '/scratch_net/biwidl101/aalbahou';
    else
        opts.path = 'data';    
    end

    
    opts.imgPath = strcat(opts.path,'/',name);
    opts.phtPath = strcat(opts.path,'/data/pht_data/',name,'/');
    opts.phtPath = convertStringsToChars(opts.phtPath);
    opts.phtPathAmp = [opts.path,'/data/pht_data/',name,'/pht_amp_',name,'.mat'];
    %opts.scatter_map = convertStringsToChars(strcat('/home/aalbahou/phantoms/',name));
    opts.scatter_map = convertStringsToChars(strcat('/home/aalbahou_msc/data/phantoms/',name));
    opts.rfpath = strcat(opts.path,'/data/rf_data_',name);
    opts.rfpath = convertStringsToChars(opts.rfpath);
    opts.reservation_path = '/home/aalbahou/reservations';
    
    % Directory containing input images (for evaluation mode)
    % opts.input_dir = '/scratch_net/biwidl103/aalbahou/masterthesis/distributed_sim/input_dir';
    %opts.input_dir='/scratch_net/biwidl103/aalbahou/masterthesis/image_editing_lib/selected/outputs/resampled';
    %opts.input_dir = '/scratch_net/biwidl103/aalbahou/training_results/pix2scat_ct_oct/final_product/cyst_phantom_5mhz_m30_p30_10perc';
    %opts.input_dir = '/scratch_net/biwidl103/aalbahou/training_results/pix2scat_ct_oct/final_product/random_in_vivo';
    opts.input_dir = '/scratch_net/biwidl103/aalbahou/training_results/pix2scat_ct_oct/final_product/blabla';
    %opts.input_dir = '/scratch_net/biwidl103/aalbahou/training_results/3pix2scat_ct_oct/final_product/cyst_5mhz_m10_p10';

    % Create output directories
    if ~exist(opts.rfpath,'dir')
        mkdir(opts.rfpath);
    end
    for probe_angle = opts.probe_angles*360/(2*pi)
        if ~exist(strcat(opts.rfpath,'/',num2str(probe_angle)),'dir')
            mkdir(strcat(opts.rfpath,'/',num2str(probe_angle)));
        end
    end
    if ~exist(opts.phtPath,'dir')
        mkdir(opts.phtPath);
    end
    if ~exist(opts.scatter_map,'dir')
        mkdir(opts.scatter_map);
    end
    % =======================================================================================================

end % End opts