# Andrawes Al Bahou (c). September 2018. 
#
# FUNCTIONALITY:
# This program (JobAllocator) is a master node responsible for bookkeeping. 
# It keeps a list of all tasks which must be run by the MATLAB threads. It
# receives requests from the different matlab jobs, and as a response, 
# assigns them a simulation task. 
# JobAllocator is there to make sure that there is no overlap in the work 
# done by the MATLAB threads which are assumed not to be able to communicate
# nor to have a shared filesystem.
# JobAllocator acts as an API request handler on port 6000. (Note that the
# MATLAB threads must be inside the ETH Network, or exist there through an 
# SSH port forward.) 
#
# *NOTE: Currently a problem with the API server is causing a problem 
#        Therefore, instead of communicating over web sockets with the
# 		 Matlab threads, they write empty files to specified directory 
#		 accessible by the threads (/home/aalbahou/). The filenames of those
#		 empty files are the text strings which are communicated to the MATLAB
#		 threads. This will be fixed in the next release of JobAllocator.
#
# JobAllocator creates frequent backups. In case of failure, it can be 
# restarted from a previous backup using the --restore switch.
#
# JobAllocator reallocates jobs which have not been completed within a certain
# timeframe (10 min).
#
# JobAllocator can run in 4 modes: 
# t1: Generation of tissue phantoms
# t2: Simulation of US interaction with tissue phantoms
# td: At the end of t2, running in td mode allows the user to check if any
#     tasks have not been completed (due to corrupt files, terminated 
#     threads, etc.) and completes these tasks
# t3: US b-mode image creation from the simulated data 


# TODO: Change the while available clause because the program shuts down when the available list is 
# empty but should be when the complete list is full, or when there are no pending items in the pending list

# This script manages contention on the tasks of my distributed Field II simulation job 
import os
import os.path 
import sys 
import time
import shutil
import re
import pickle
import argparse


# ===============================================================================================================================
# Helper Functions
# ===============================================================================================================================
# Implementation of touch in Python
def touch(fname):
    try:
        os.utime(fname, None)
    except OSError:
        open(fname, 'a').close()
# Routine to serialize and store object (in binary)
def save_obj(obj, name ):
    with open(name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
# Routine to lead object
def load_obj(name ):
    with open(name, 'rb') as f:
        return pickle.load(f)
# Routine to write acknowledgement of job allocation
def write_ack(job_request_folder, experiment_name, rf_line, depth, angle, thread_id, job_type):
    # Name of the acknowledgement file
    acknowledgement_file_name = job_request_folder+'/'+'granted_'+thread_id+'_'+job_type+'_'+experiment_name+'_'+rf_line+'_'+depth+'_'+angle+'.mat'        
    # Write acknowledgement file
    touch(acknowledgement_file_name)
# Routine to remove the acknowledgement-of-job-allocation signal
def remove_ack(job_request_folder, experiment_name, rf_line, depth, angle, thread_id, job_type):
    # Name of the acknowledgement file
    acknowledgement_file_name = job_request_folder+'/'+'granted_'+thread_id+'_'+job_type+'_'+experiment_name+'_'+rf_line+'_'+depth+'_'+angle+'.mat'
    
    try:
        # Remove the corresponding acknowledgement file
        os.remove(acknowledgement_file_name)
    except OSError:
        print("There is no such granted job: "+str((thread_id,job_type,experiment_name,rf_line,depth,angle)))
# Routine to remove signals originated from dead or irresponsive threads
def clean_stale_messages(job_request_folder, available, pending):
    # Delete files older than 10 minutes (assuming the thread concerned has died)
    for file in os.listdir(job_request_folder):
        if file.startswith('granted'):
            st=os.stat(job_request_folder+'/'+file)
            # Extract last modification time
            mtime=st.st_mtime
            if (time.time()-mtime > 10*60):
		# Extract the thread ID concerned by this stale request
		data = (file.split('.')[0]).split('_')
		thread_id = data[1]

                # Delete the stale signal written by matlab process
                os.remove(job_request_folder+'/'+file)  

                # Remove corresponding entry from pending
                stale_job = pending[thread_id]
                del pending[thread_id]

                # Place the job back in "available"
                available.append(stale_job)
# Routine to create periodic backups
def periodic_backup(timestamp, current_session, session_file):
    current_time = time.time()
    if (current_time-timestamp) > 120: # Create backup every 120 sec
        save_obj(current_session, session_file)
        # This second file is for failure mode recovery
        save_obj(current_session, session_file+'.pkl')
        print("Created backup.")
        # Update timestamp
        timestamp = time.time()

    return timestamp 
# ===============================================================================================================================



# ===============================================================================================================================
# JobAllocator principal functions. The two functions here initialize JobAllocator and run a round of signal handling
# ===============================================================================================================================
# Signal handling: Decode the signal send to JobAllocator and take action (depending on the action keyword)
def job_allocator(job_request_folder, available, pending, finished): 

    # Scan over all files (signals)
    for file in os.listdir(job_request_folder):
        if file.endswith('.mat'): 
            # Read and decode signal data
            signal_data = (file.split('.')[0]).split('_')
            action = signal_data[0]
            thread_id = signal_data[1]

            # Request Job
            if action == 'request':                 
                # Initialize new_job variable
                new_job = None
                
                # Make sure the current thread_id is not already running a job 
                if thread_id not in pending.keys(): 
                    try:
                        # Fetch a new job to assign from the available jobs list
                        new_job = available.pop()
                        #while new_job[1] in blacklist :
                            #new_job = available.pop()

                        # Place it in the pending jobs dictionary
                    	pending[thread_id] = new_job
                    	# Unpack information
                    	job_type = new_job[0]
                    	experiment_name = new_job[1]
                    	rf_line = new_job[2]
                    	depth = new_job[3]
                    	angle = new_job[4]

                    	# Write request acknowledgement 
                    	write_ack(job_request_folder, experiment_name, rf_line, depth, angle, thread_id, job_type)
                                                   
                    except IndexError: #This exception means the list is empty 
                        pass
                        #print("No more tasks available. Waiting for remaining jobs to finish execution before terminating JobAllocator.")
                        

                else :
                    print("The threadID: "+str(thread_id)+" corresponds to a pending job(",str(pending[thread_id]),"). This request will be ignored.")
                
                # Delete the "request" signal written by matlab process
                os.remove(job_request_folder+'/'+file)
            
            # Remove from pending jobs
            elif action == 'remove':
                # Search for id in dictionary
                try : 
                    # Remove job from pending dictionary
                    finished_job = pending[thread_id]
                    del pending[thread_id]
                    
                    # Unpack information
                    job_type = finished_job[0]
                    experiment_name = finished_job[1]
                    rf_line = finished_job[2]
                    depth = finished_job[3]
                    angle = finished_job[4]

                    # Place in finished jobs dictionary (indexed by experiment name)
                    finished[experiment_name]= finished_job

                    # Remove the acknowledgement file
                    remove_ack(job_request_folder, experiment_name, rf_line, depth, angle, thread_id, job_type)
                    
                except KeyError: # If the entry in the dictionary does not exist
                    print("No corresponding entry may be removed from the allocation book. This request will be ignored")
                # Delete the "remove" request file written by matlab process
                os.remove(job_request_folder+'/'+file)

            # Action is to grant request (written by this program)
            elif action == 'granted' :
                # Do nothing
                pass 

            # Action keyworkd is undefined
            else :
                print("Undefined keyword !: "+action+". The file will be removed")
                # Remove signal file written by matlab process
                os.remove(job_request_folder+'/'+file)

	    # Remove jobs which have been pending for too long
            clean_stale_messages(job_request_folder, available, pending)


    # This interval to limit disk IO
    time.sleep(0.2) 
# Routine to startup the JobAllocator program
def init_job_allocator():
    print("")
    print("JobAllocator v1.0 is now active")
    print("")
    
    
    # Create a command line argument parser
    parser = argparse.ArgumentParser(description='JobAllocator. Manage task allocation for asynchronous distributed tasks.')
    parser.add_argument('--restore', action="store_true", default=False, help="Restore a previous session.")
    parser.add_argument('--dir', action="store", default=os.getcwd(), help="FULL path of the directory where the job requests are written.")
    parser.add_argument('--databook', action="store", default=os.getcwd()+'/obj/databook.pkl', help='If restore option is set, this indicates the location (FULL PATH) of the restoration file (.pkl) to load from, otherwise, this indicates the location (FULL PATH and name including extension) where to store the restoration file.')
    parser.add_argument('--t1', action="store_true", default=False, help='Run Part 1 of the processing (scatter map generation)')
    parser.add_argument('--t2', action="store_true", default=False, help='Run Part 2 of the processing (US simulation of RF lines)')
    parser.add_argument('--t3', action="store_true", default=False, help='Run Part 3 of the processing (image rendering)')
    parser.add_argument('--td', action="store_true", default=False, help='Debugger mode to display unfinished/failed tasks from t2')
    parser.add_argument('--fix', action="store_true", default=False, help='When in debugger mode, adding this flag relaunches the failed/leftover tasks from t2')
    clargs = parser.parse_args()
    
    
    job_request_folder = clargs.dir
    session_file = clargs.databook
    # Directory where job requests are written
    print("Active directory set to: "+job_request_folder)
    print("Session restoration file set to: "+session_file)

    return (job_request_folder, session_file, clargs)
# Routine to load a previous session
def init_jobs(job_request_folder, session_file, clargs):
    # If the option is set to restore a previous session
    if clargs.restore == True:
        # Load the previous session
        try:
            print("Wait for previous session to be restored...")
            previous_session = load_obj(session_file)
            available = previous_session[0]
            pending = previous_session[1]
            finished = previous_session[2]
            print("Session successfully restored.")
        except IOError:
            print("File: "+session_file+ " does not exist")
            # Initialize data structures which will contain the available, pending and finished jobs
            available = list()
            pending = dict()
            finished = dict()


    else : # If it's a new session
        # Clear all the acknowledgement and removal requests
        for file in os.listdir(job_request_folder):
            if file.startswith('granted') or file.startswith('remove'):
                os.remove(job_request_folder+'/'+file)

        # Clear any previous session restoration files
        try: 
            os.remove(session_file)
        except OSError:
            pass

        # Initialize data structures which will contain the available, pending and finished jobs
        available = list()
        pending = dict()
        finished = dict()

    return(available, pending, finished)
# ===============================================================================================================================



# ===============================================================================================================================
# Main function 
# ===============================================================================================================================
def main():
    try:
        # ----------------------------------------------------------------------------
        # Job settings 
        # ----------------------------------------------------------------------------
        n_rf_lines = 410;       # Number of rf_lines in the simulation
        angles_start = -0;     # Initial angle
        angles_stop = 0;       # Final angle
        angles_step = 1;       # Interval size between consecutive angles

        n_slices = 1;           # Number of slices which we image (for 3D volumes)

        force_angle = False;     # Force angle to take the value given by the file name
        
        # Create a list with the names of all the US simulation experiments to run
        exp_names_dir = '/home/aalbahou/experiments'
        experiment_names = [file.split('.')[0] for file in os.listdir(exp_names_dir)]
        # ----------------------------------------------------------------------------

        # Initialize JobAllocator
        (job_request_folder, session_file, clargs) = init_job_allocator()
        # Initialize datastructures containing job infor
        (available, pending, finished) = init_jobs(job_request_folder, session_file, clargs)
            
        timestamp = time.time()

        if clargs.t1 == True: # Run task 1
            if clargs.restore == False :
                # Populate "available" list for task 't1'
                for exp in experiment_names:
                    new_job = ('t1',exp, '0','0','0') 
                    available.append(new_job)
    
            # Run JobAllocator until tasks of type t1 are complete ("available" becomes empty list)
            while (available) or (pending) :
                job_allocator(job_request_folder, available, pending, finished)
                # Create backup if enough time has elapsed since last backup
                current_session = (available, pending, finished)
                timestamp = periodic_backup(timestamp, current_session, session_file)
        
        if clargs.t2 == True: # Run task 2
            if clargs.restore == False :
                # Populate "available" list for task 't2'
                for line in range(1, n_rf_lines+1):
                    for angle in range(angles_start, angles_stop+1, angles_step):
                        for slice_y in range(1, n_slices+1):
                            for exp in experiment_names:
                                new_job = ('t2', exp, str(line), str(slice_y), str(angle))
                                available.append(new_job)

            # Run JobAllocator until tasks of type t2 are complete ("available" becomes empty list)
            while (available)  or (pending) :
                job_allocator(job_request_folder, available, pending, finished)
                # Create backup if enough time has elapsed since last backup
                current_session = (available, pending, finished)
                timestamp = periodic_backup(timestamp, current_session, session_file)


        if clargs.td == True: # Run task debugger
            if clargs.restore == False :
                # Populate "available" list for task 'td'
                for line in range(1, n_rf_lines+1):
                    for angle in range(angles_start, angles_stop+1, angles_step):
                        for slice_y in range(1, n_slices+1):
                            for exp in experiment_names:
                                new_job = ('t2', exp, str(line), str(slice_y), str(angle))
                                available.append(new_job)

            new_available = list()
            # Check which files still have not been generated
            for job in available:
            	#basedir = '/scratch/aalbahou/dataset/distributed_sim/data/data/rf_data_'
                #basedir = '/srv/glusterfs/aalbahou/common_dir/data/rf_data_'
                basedir = '/scratch/aalbahou/data/rf_data_'
                if force_angle == True:
                    angle = (job[1].split('^'))[1]
                    file_in_question = basedir+job[1]+'/'+angle+'/rf_pos_'+job[3]+'_ln_'+job[2]+'.mat'
                else:
                    file_in_question = basedir+job[1]+'/'+job[4]+'/rf_pos_'+job[3]+'_ln_'+job[2]+'.mat'
            	
                if not os.path.isfile(file_in_question):
            		print(file_in_question)
            		new_available.append(job)

            if clargs.fix == True:
                available = new_available
                # Run JobAllocator until tasks of type td are complete
                while (available)  or (pending) :
                    job_allocator(job_request_folder, available, pending, finished)
                    # Create backup if enough time has elapsed since last backup
                    current_session = (available, pending, finished)
                    timestamp = periodic_backup(timestamp, current_session, session_file)


        if clargs.t3 == True: 
            if clargs.restore == False :
                # Populate "available" list for task 't3'
                for angle in range(angles_start, angles_stop+1, angles_step):
                    for slice_y in range(1, n_slices+1):
                        for exp in experiment_names:
                            new_job = ('t3', exp, '1', str(slice_y), str(angle))
                            available.append(new_job)        
    
            # Run JobAllocator until tasks of type t3 are complete
            while (available)  or (pending) :
                job_allocator(job_request_folder, available, pending, finished)
                # Create backup if enough time has elapsed since last backup
                current_session = (available, pending, finished)
                timestamp = periodic_backup(timestamp, current_session, session_file)

        print('Thank you for using JobAllocator ! :D ')

    except KeyboardInterrupt: 
        print("Dumping the session information in "+session_file)
        try:
            # Serialize and save the current session
            current_session = (available,pending,finished)
            save_obj(current_session, session_file)
            # This second file is a failure mode backup
            save_obj(current_session, session_file+'.pkl') 
            print('Thank you for using JobAllocator ! :D ')
        except UnboundLocalError:
            print('ERROR ! Cannot dump the session information on exit because data has not been loaded yet!')
# ===============================================================================================================================



if __name__=="__main__":
    main()
