% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% Functionality : This function plots US scatter pattern phantom, or 
%                 saves it as image
% INPUT : 
%     pos     :   Positions of scatterers.
%                 This is a matrix of phantom scatterer positions (X,Y,Z)
%     amp     :   Scatterer amplitudes.
%                 This is a vector of phantom scatterer amplitudes (same number
%                 as entries in Phantom positions)
%     x_size  :   Width of phantom [mm]
%     z_size  :   Height of phantom [mm]
%     z_start :   Start of phantom surface [mm]
%     titl    :   Plot title
%     exper   :   Experiment directory

function [] = plotPhantom(pos, amp, x_size, z_size, z_start, titl, exper)

    f = figure;
    axes1 = axes('Parent',f);
    hold(axes1,'on');
    scatter(pos(:,1)*1e3,pos(:,3)*1e3,1,amp,'linewidth',3);
    grid on; colormap('gray');
    set(gca,'YDir','reverse');
    axis equal;
    xlim([-x_size/2 x_size/2]*1e3); 
    ylim([z_start min(z_start+z_size,0.085)]*1e3);
    xlabel('x-position [mm]');
    ylabel('z-position [mm]');
    title(titl);
    set(axes1,'Color',[0 0 0],'DataAspectRatio',[1 1 1]);
    titl(regexp(titl,'[ ]')) = ['_'];
    saveas(f, strcat(exper,'_',titl,'.png'),'png'); 
    
end