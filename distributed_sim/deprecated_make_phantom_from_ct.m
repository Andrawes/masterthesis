% Deprecated. 
% This function creates a grid of scatterers, and the amplitude associated with each scatterer to create 
% an US phantom. The features in the phantom are sampled from real-life medical imaging data (Not US, 
% but CT).

function [x_s, y_s, z_s, amplitudes] = make_phantom_from_ct(opts)

    % ==================================================================================================
    % Now we create a regular grid of scatterers
    % ==================================================================================================    
    
    % phantom size
    x_size = opts.x_size;       %  Width of phantom [m]
    y_size = opts.y_size;       %  Transverse width of phantom [m]
    z_size = opts.z_size;       %  Height of phantom [m]
    z_start = opts.z_start;     %  Start of phantom surface [m];
    nbrSpeckles = opts.N;       %  Number of speckles

    
    % Create the speckle positions randomly
    if (opts.surrounding_context == true)
        x_s = rand(round(nbrSpeckles),1)*x_size-x_size/2;
        y_s = rand(round(nbrSpeckles),1)*y_size-y_size/2;
        z_s = rand(round(nbrSpeckles),1)*z_size+z_start;
    else 
        x_s = rand(round(nbrSpeckles),1)*x_size/opts.x_scale_factor-(x_size/opts.x_scale_factor)/2;
        y_s = rand(round(nbrSpeckles),1)*y_size-y_size/2;
        z_s = rand(round(nbrSpeckles),1)*z_size/opts.z_scale_factor+z_start+opts.circ_offset_z;
    end
  
    
    % Distance (in the Z-axis) between the inscribed rectangle, and outscribed square.    
    circ_offset_z = opts.circ_offset_z;

    
    % density coef such that {x,y,z}_levels = density_coef * {x,y,z}_size; 
    density_coef = (nbrSpeckles/(x_size*y_size*z_size))^(1/3);
    
    % Compute average separation distance between 2 neighboring grid vertices in [mm]
    delta = 1/(density_coef);
    delta_x = delta;
    delta_y = delta;
    delta_z = delta;
 
    % ==========================================================================================

    
    

    %%%%%%%%% Create discretization grid %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
    % Width of the discretization grid on each axis
    %width_x = 109;
    width_x = 153;
    width_y = 14; 
    %width_z = 163;
    width_z = 192;
       
    % Create discretization grid
    if (opts.surrounding_context == true)
        % Increments in [mm] between adjacent grid vertices
        delta_xg = x_size/(width_x-1);
        delta_yg = y_size/(width_y-1);
        delta_zg = z_size/(width_z-1);   
        % Grid axes
        grid_base_x = (-(x_size)/2:delta_xg:(x_size)/2)'; 
        grid_base_y = (-(y_size)/2:delta_yg:(y_size)/2)';
        grid_base_z = (z_start+(0:delta_zg:z_size)');
    else
        % Increments in [mm] between adjacent grid vertices
        delta_xg = (x_size/opts.x_scale_factor)/(width_x-1);
        delta_yg = y_size/(width_y-1);
        delta_zg = (z_size/opts.z_scale_factor)/(width_z-1);   
        % Grid axes
        grid_base_x = (-(x_size/opts.x_scale_factor)/2:delta_xg:(x_size/opts.x_scale_factor)/2)'; 
        grid_base_y = (-(y_size)/2:delta_yg:(y_size)/2)';
        grid_base_z = (z_start+opts.circ_offset_z+(0:delta_zg:(z_size/opts.z_scale_factor))');
    end
    
    % Discretization factor
    discretization_factor_x = delta_xg/delta
    discretization_factor_y = delta_yg/delta
    discretization_factor_z = delta_zg/delta
    

   % TODO adjust meshgrid such that the points lie in the middle of the cubic regions
   % Not a big problem probably
    [x_g, z_g, y_g] = meshgrid(grid_base_x, grid_base_z, grid_base_y);

    % ==================================================================================================




    % ==================================================================================================
    % Now we create the visual features by sampling CT data or by creating random ellipses
    % ==================================================================================================

    % Load external volume
    %[vol, information] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig10_pv.mha');
    %dimensions = [0.625, 0.625, 0.301025]*1000 ; % In [mm]
    %[vol, information] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig11_pv.mha');
    % Dimensions [0.625, 0.625, 0.301025]
    %[vol, information] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig12_pv.mha');
    % Dimensions [0.625, 0.625, 0.300049]
    %[vol, information] = readMHA('/home/aalbahou_msc/data/pigLiverCT3D/Pig13_pv.mha');
    % [0.625, 0.625, 0.300049]

    % Randomly chose which source file to fetch the feature data from   
    source_file = floor(rand()*(3));
    %source_file = 2;
    
    if (source_file == 0)
        [vol, information] = readMHA('/home/aalbahou_msc/data/shoulderCT3D/509961CT.mha');
        dimensions = [0.382813, 0.382813, 0.5] ;% In [m]
        
        % This zoom_out factor describes how much the scale of the scatterer
        % space is increased in order to match the scale in the CT space where
        % resolution is worse than in US.
        zoom_out_factor = floor(rand()*2)+3;
        %display('using volume 1')
    
    elseif (source_file == 1)
        [vol, information] = readMHA('/home/aalbahou_msc/data/visceralCT3D/10000005_1_CT_wb_superiorROI.mha');
        dimensions = [0.818359, 0.818359, 1.5] ; % In [m]
        zoom_out_factor = floor(rand()*3)+6;
        %display('using volume 2')
    else 
        [vol, information] = readMHA('/home/aalbahou_msc/data/visceralCT3D/10000005_1_CT_wb_superiorROI.mha');
        vol = permute(vol,[3,2,1]);
        dimensions = [1.5, 0.818359, 0.818359] ; % In [m]
        zoom_out_factor = floor(rand()*3)+4;
        %display('using volume 3')
    end

    [x_dim z_dim y_dim] = size(vol); % Resolution in pixels
    delta_pix_x = dimensions(1)/x_dim; % Distance between pixels in [m] (x-axis)
    delta_pix_z = dimensions(2)/z_dim; % Distance between pixels in [m] (z-axis)
	delta_pix_y = dimensions(3)/y_dim; % Distance between pixels in [m] (y-axis)    

    % Find the maximum offset of pixel positions in the circle in each dimension 
    right_edge_offset = round(zoom_out_factor*(max(x_g(:))+(x_size/2))/delta_pix_x)+1;
    bottom_edge_offset = round(zoom_out_factor*(max(z_g(:))-z_start)/delta_pix_z)+1;
    front_edge_offset = round(zoom_out_factor*(max(y_g(:))+(y_size/2))/delta_pix_y)+1;

    % Just making sure that an offset can actually be found.
    if (bottom_edge_offset >= length(vol(:,1,1)) || right_edge_offset >= length(vol(1,:,1)))
        display('You might need to reconsider the zoom out factor');
        display(bottom_edge_offset)
        display(length(vol(:,1,1)))
        display(right_edge_offset)
        display(length(vol(1,:,1)))
        %NEW
        display(front_edge_offset)
        display(length(vol(1,1,:)))
    end

    % Find Random window offsets
    window_offset_x = round(rand()*(length(vol(1,:,1))-right_edge_offset));        
    window_offset_z = round(rand()*(length(vol(:,1,1))-bottom_edge_offset));
    window_offset_y = round(rand()*(length(vol(1,1,:))-front_edge_offset));


    % Sample the CT volume to create the high-level features (in 2D)
    features = vol(sub2ind(size(vol),...
    		   			  round(zoom_out_factor*(z_g-z_start)/delta_pix_z)+1+window_offset_z,...
    		   			  round(zoom_out_factor*(x_g+(x_size/2))/delta_pix_x)+1+window_offset_x,...
						  round(zoom_out_factor*(y_g+(y_size/2))/delta_pix_y)+1+window_offset_y));


    % Scale features such that their values (amplitudes) are between 0 and 1
    maximum = double(max(vol(:)));
    minimum = double(min(vol(:)));
    features = double(features);
    features = (features-minimum)/(maximum-minimum);


    % Add noise (between 70% and 100%)
    %lambda = 0;
    lambda = 0.7+rand()*0.3;
    feature_dims = size(features);
    pixels = (rand(feature_dims(1), feature_dims(2), feature_dims(3)).*features)*(lambda)+ features*(1-lambda) ;


	% Trilinear interpolation of the speckle amplitudes from the discrete grid
    amplitudes = interp3(x_g, z_g, y_g, pixels, x_s, z_s, y_s);
   
    % Create a circular mask
    % The 1e-7 was added to solve a small numerical problem whereby two small floats 
    % should be equal in theory but are not in practice
    circle_mask =  (((x_s-opts.r_c_x).^2 + (z_s-opts.r_c_z).^2 ).^(1/2) <= x_size/2+1e-7);
    % Remove all scatterers which are not in the circle
    x_s = x_s(circle_mask);
    y_s = y_s(circle_mask);
    z_s = z_s(circle_mask);
    
    amplitudes = amplitudes(circle_mask);
    positions = [x_s, y_s, z_s];

	
    %plot
    plotPhantom(positions, amplitudes, opts.x_size, opts.z_size,...
               opts.z_start, 'Speckle Phantom', opts.imgPath);
    
     % ==================================================================================================
           
           
           
           
	%%%%%%%% WRITE OUT THE IMAGES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

	% Rectangle mask to take into account only the rectangular region which will be used
    rectangle_mask = (x_g >= (-x_size/opts.x_scale_factor)/2 & x_g <= (x_size/opts.x_scale_factor)/2);
    rectangle_mask = rectangle_mask & ((z_g >= circ_offset_z+z_start) & (z_g <= circ_offset_z+z_start+z_size/opts.z_scale_factor) );

   
    size(pixels)
    
    % Select a rectangular subset of the scatterers to output into an image file
	pixels = pixels(rectangle_mask);

    size(pixels)
    
	% These calculations come from the written down sheet (Archive1) which also contains
	% references to the variable names used here
	Q = -(x_size/opts.x_scale_factor)/2-min(grid_base_x(:));
	R = (x_size/opts.x_scale_factor)/2-min(grid_base_x(:));
    
    
    if (Q/delta_xg == floor(Q/delta_xg))
		no1 = Q/delta_xg;
    else
		no1 = floor(Q/delta_xg)+1;
    end

    nk = floor(R/delta_xg)+1;
   
	ni = nk-no1;

    
	% Rehsape pixels into 2D image (with multiple channels)
	pixels = reshape(pixels, [], ni, length(grid_base_y));

	% Save individually all channels
	for c = 1:length(grid_base_y)
		% Write the resulting image to the output file
    	imwrite(pixels(:,:,c),strcat(opts.phtPath,opts.experiment_name,'c',num2str(c),'.png'),'png' );
	end

    %figure
    %pixpix = reshape(pixels, [length(grid_base_z), length(grid_base_x), length(grid_base_y)]) ;
    %imshow(pixels(:,:,1) )


%    % Save
%    save(opts.phtPathAmp,'amplitudes');
%    
%    % ==================================================================================================

end
