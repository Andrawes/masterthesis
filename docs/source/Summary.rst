Summary
-----------------------------------------

This subdirectory contains the matlab sourcode framework used during the project for two purposes: 

1. Dataset generation 
2. Simulation of B-mode images (from pre-existing scatter patterns)

This subdirectory also contains the sourcecode for the JobAllocator (``job_allocator.py``) which allocates simulation jobs to all the running threads on the cluster.
