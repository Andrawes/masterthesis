ReportLatex
=========================================

This subdirectory contains the source files and Latex Documents for compiling the final report of this master thesis. To compile the master thesis, run: 

``./compile_report.sh``

This will compile SVG images into PDF files, it will create the glossary, the bibliography and produce a file named 'main.pdf'.
