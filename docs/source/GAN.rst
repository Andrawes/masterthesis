GAN
=========================================

This subdirectory contains the files relevant to the Tensorflow Model of the Neural Network. The ``scatgan1``, ``scatgan3`` and ``vol2scat`` subdirectories contain: 

- The (untrained) Tensorflow model for the network 
- The training and validation datasets
- The Testing dataset on which the quantitative evaluations are run (Report and Paper)

respectively for the *scatgan1*, *scatgan3* networks, and the network which processes full 3D volumes (*vol2scat*).


Sample Commands to Use the PIX2PIX GAN
--------------------------------------------

The following commands allow one to:

**Concatenate {Input, Target}-image pairs**

.. code-block:: html

  python tools/process.py \
    --input_dir <input_image_dir> \
    --b_dir <target_image_dir> \
    --operation combine \
    --output_dir <dataset_dir_name>


**Split into train/validation set**

.. code-block:: html

  python tools/split.py \
    --dir <dataset_dir_name>


**Train Network** 

.. code-block:: html

  python pix2pix.py \
    --mode train \
    --output_dir <dataset_dir_name>_train \
    --max_epochs 1000 \
    --input_dir <dataset_dir_name>/train \
    --which_direction AtoB \
    --display_freq 50


**Test/Predict**

.. code-block:: html

  python pix2pix.py \
  --mode test \
  --output_dir <test_output_dir> \
  --input_dir <dataset_dir_name>/val \
  --checkpoint <dataset_dir_name>_train


**Tensorboard**

.. code-block:: html

  tensorboard --logdir=<dataset_dir_name>_train --port=6006