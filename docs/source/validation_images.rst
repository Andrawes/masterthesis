validation_images
=========================================

This subdirectory contains **input** test images (B-mode) used in the paper and report to compute performance metrics for our pipeline (ScatGan). It contains both *in vivo* and *in silico* B-mode images. The subdirectory also contains **reconstruction** images (B-mode) obtained after simulating the scatter patterns produced by the neural network.