docs
=========================================

This subdirectory contains the documentation for this project. Created with Sphinx, and hosted on ReadTheDocs. 
To compile documentation run: 

``make html``

in the ``docs`` directory. 
