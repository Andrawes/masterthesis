metrics
=========================================

This subdirectory contains the matlab code used to compute error between two sets of B-mode images.

To use it, simply run the "compare.m" script in matlab:

``matlab -r "compare"``

Make sure to set the directory of the ground truth images (``gt_dir``), the directory of the reconstruction images (``rec_dir``). The code performs a pairwise comparison, so the images in each directoy must be named identically for the routine to know which two to compare. The output is an html file (name can be set with the variable ``results_path``) summarizing the comparison. 

Make sure to set the option ``sweep`` to ``true`` if performing the evaluation on the cyst data, in order to obtain the plots of error vs probe angle. Finally setting the option ``one_to_many`` to ``true`` allows a comparison of one ground truth image to many reconstruction images.