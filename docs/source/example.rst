Running A Distributed Simulation (Example)
-------------------------------------------

In order to run the simulation environment: 

1. **Create 10 Experiment Files** (This defines the names and number of simulation experiments to run)

.. code-block:: sh

  export EXPERIMENT_DIR=/home/$USER/experiments
  cd $EXPERIMENT_DIR
  touch experiment{01..10}.exp

.. note:: 
  Experiment names should not contain underscores ('_'), and must have the extension '.exp'.


2. **Set a Reservation Directory** (*JobAllocator communicates with the matlab processes by writing files to this directory [1]*):

.. code-block :: sh

  export RESERVATION_DIR=/home/$USER/reservations/

.. note:: 
  It is important that ``RESERVATION_DIR`` is accessible from all runnning instances of the distributed code. This is why it is advisable to run it on the ``home`` directory, which at D-ITET is accessible from all machines.

3. **Launch JobAllocator**

.. code-block:: bash

  python job_allocator.py --dir $RESERVATION_DIR --databook $RESERVATION_DIR/obj/databook.pkl --t1

.. note:: 

  For more information on JobAllocator switches and options, run ``python job_allocator.py --help`` 

4. **Deploy and run jobs on the cluster**

.. code-block :: bash

  ./job_submission.sh

.. [1] JobAllocator and the matlab process currently communicate with each other by writing files to this directory. Note that this mechanism is a temporary workaround. Initially, the communication was meant to happen over HTTP through REST calls to JobAllocator (which services API calls), as documented in the report.