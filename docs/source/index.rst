Master Thesis codebase documentation
=========================================

Welcome to the main page for the documentation of the codebase of my master thesis: 

**Ultrasound Tissue Scatterer Reconstruction using GANs**, *by Andrawes Al Bahou*.

This work has been undertaken at ETH Zurich, at the Deparment of Electrical Engineering, in the Computer Vision Lab (CVL), under *Prof. Dr. Orcun Goksel*, with the supervision of *Dr. Christine Tanner*. 

The following list shows the documentation for each subdirectory in the repository. You can click through or use the search functionality. 


.. toctree::
   :maxdepth: 3
   :caption: Repository Subdirectories:

   distributed_sim
   GAN
   metrics
   validation_images
   ReportLatex
   cluster_scripts
   docs

.. note::

  Undocumented subdirectories are not part of the Master Thesis, and were developed for personal use.
