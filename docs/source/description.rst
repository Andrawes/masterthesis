Description 
--------------------------------------------

job_finished.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality** This function is used to interact with JobAllocator. At the end of a certain job, the thread can use this function to signal to JobAllocator that the task has been accomplished.

.. note:: 
  Due to a problem with the API, this method is currently implemented by writing an empty file, whose name is the signal to be sent to JobAllocator (JobAllocator then scans for these files.)

+-----------+---------------------------------------------------------------------------------------+
| **Input** |  :res_path:  (string) Directory where the message files to Job Allocator are written  | 
|           |  :thread_id: (string) ID of the current thread                                        |    
+-----------+---------------------------------------------------------------------------------------+

main.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

|  **Functionality** Main function of the ultrasound simulation routine (built mainly on top
|  of the FieldII US simulation program). When using this program in a distributed
|  setting, make sure you are using it in compiled mode, as it requires no MATLAB
|  licence.
|  This function takes in some configuration flags which setup the running mode of 
|  the simulator. Mainly, they determine whether to run in data generation
|  mode, or scatterer generation mode. In the former, random pairs of
|  {scatter-map; b-mode image} are generated. In the latter, bmode images
|  are simulated for predefined scatterers. After configuring the simulator, 
|  it enters an infinite loop where it requests simulation tasks from JobAllocator,
|  then runs them. 

+----------+------------------------------------------------------------------------------------------------------------------------------------+
|**Input** | :thread_id:  (string) ID of the current thread. This can be passed as the first command line argument when running in compiled mode|
+----------+------------------------------------------------------------------------------------------------------------------------------------+

make_cyst.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality**
This function creates a cyst scatter map. It is a circular inclusion of lesser amplitude.

+----------+--------------------------------------------------------------------------------------------------------------+
|**Input** | :opts:               Options structure for this simulation                                                   |                                 
|          | :fast_run:           Flag. Used in debugging for running the US simulation quickly                           |                                                         
|          | :evaluation_mode:    Flag. If true, means the program is in evaluation mode, else in dataset generation mode.| 
|          | :sim2d:              Flag. If true, indicates the program is a 2D simulation (not 3D)                        |                                                            
|          | :new_scatmap:        Flag. If true, indicates that a new random scatter-map can be generated.                |                                                                    
+----------+--------------------------------------------------------------------------------------------------------------+
|**Output**| :x_s:                Vector of x-coordinates of the scatterers                                               |                
|          | :y_s:                Vector of y-coordinated of the scatterers                                               |                
|          | :z_s:                Vector of z_coordinated of the scatterers                                               |                
|          | :amplitudes:         Vector of amplitudes of the scatterers                                                  |             
+----------+--------------------------------------------------------------------------------------------------------------+

make_ellipsoids.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality**
This function takes a regular 3D grid of points, and assigns them amplitudes. It does this by creating randomly-sized and rotated ellipsoids in that 3D space. The distance from the center of these ellipsoids describes a random spatial gradient. This spatial gradient is then used to assign amplitudes to the points in space.

+-----------+---------------------------------------------------------------------------------------------------+
|  **Input**| :opts:       Options structure for this simulation.                                               |                     
|           | :x_size:     Lateral size of the phantom                                                          |          
|           | :y_size:     Elevation size of the phantom                                                        |            
|           | :z_size:     Axial size of the phantom                                                            |         
|           | :z_start:    Distance of the phantom from the transducer                                          |                          
|           | :x_g:        Vector of x_coordinates of the points of the input grid                              |                                      
|           | :y_g:        Vector of y_coordinates of the points of the input grid                              |                                      
|           | :z_g:        Vector of z_coordinates of the points of the input grid                              |                                      
+-----------+---------------------------------------------------------------------------------------------------+
| **Output**| :multiplicative_amplitude :  Vector of amplitudes assigned to the points of the input 3D grid     |
+-----------+---------------------------------------------------------------------------------------------------+                               

make_image.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality**
This function takes a number of RF lines, places them together, and performs some additional processing to extract a B-mode image, or an RF-image, and saves them.

+----------+--------------------------------------------------------------------------------+
|**Input** | :opts:           Options structure for this simulation                         |    
|          | :probe_angle:    Angle in radians of US beam incidence                         |    
|          | :y_slice:        Depth of the current slice when simulating a 3D volume        |                     
|          | :render_bmode:   Flag: if true, renders a bmode image, else renders RF image   |                          
+----------+--------------------------------------------------------------------------------+

make_phantom.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality**
This function creates randomly placed scatterers (scatter map), as well as the amplitude associated with each scatterer to create a US phantom.

+------------+--------------------------------------------------------------------------------------------------------------------------+
|**Input**   | :opts:               Options structure for this simulation                                                               |
|            | :fast_run:           Flag. Used in debugging for running the US simulation quickly                                       |
|            | :evaluation_mode:    Flag. If true, means the program is in evaluation mode, else in dataset generation mode.            |
|            | :sim2d:              Flag. If true, indicates the program is a 2D simulation (not 3D)                                    |
|            | :new_scatmap:        Flag. If true, indicates that a new random scatter-map can be generated.                            |
+------------+--------------------------------------------------------------------------------------------------------------------------+
|**Output**  | :x_s:                Vector of x-coordinates of the scatterers                                                           |       
|            | :y_s:                Vector of y-coordinated of the scatterers                                                           |
|            | :z_s:                Vector of z_coordinated of the scatterers                                                           |
|            | :amplitudes:         Vector of amplitudes of the scatterers                                                              |
|            | :generate_from_ct:   Flag. Whether to use CT images, or the method of random ellipsoids for scatter map generation.      |
+------------+--------------------------------------------------------------------------------------------------------------------------+

optsInit.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality**
This function is where all the simulation settings, probe parameters, files, directories, and various other options for the FieldII simulator, and the phantom generation procedure are set.

+-----------+------------------------------------------------------------------------------------------------------------------------------------------------------------+
|**Input**  | :name:               (String). Name of the current experiment. An experiment is defined as all the different simulation settings of one particular phantom.|
|           | :sim2d:              Flag. If true, indicates the program is a 2D simulation (not 3D)                                                                      |
|           | :override_angles:    Flag. If true, indicates that the simulation angle is given in experiment name                                                        |
+-----------+------------------------------------------------------------------------------------------------------------------------------------------------------------+
|**Output** | :opts:               Options structure for this simulation                                                                                                 |
+-----------+------------------------------------------------------------------------------------------------------------------------------------------------------------+


plotPhantom.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
**Functionality** 
This function plots US scatter pattern phantom, or saves it as image

+-----------+---------------------------------------------------------------------------------------------------------------------------------------+
|**Input**  |     :pos     :   Positions of scatterers. This is a matrix of phantom scatterer positions (X,Y,Z)                                     |
|           |     :amp     :   Scatterer amplitudes. This is a vector of phantom scatterer amplitudes (same number as entries in Phantom positions) |
|           |     :x_size  :   Width of phantom [mm]                                                                                                |
|           |     :z_size  :   Height of phantom [mm]                                                                                               |
|           |     :z_start :   Start of phantom surface [mm]                                                                                        |
|           |     :titl    :   Plot title                                                                                                           |
|           |     :exper   :   Experiment directory                                                                                                 |
+-----------+---------------------------------------------------------------------------------------------------------------------------------------+

request_job_new.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality** 
This function sends a request to JobAllocator for a new simulation job. It parses the answer from JobAllocator to extract the details about the simulation job to be run, and stores this configuration in a struct called ``job``.

+------------+---------------------------------------------------------------------------------------+
| **Input**  | :res_path :  (string) Directory where the message files to Job Allocator are written  |
|            | :thread_id:  (string) ID of the current thread                                        |    
+------------+---------------------------------------------------------------------------------------+
| **Output** | :job:        job struct containg a description of the allocated simulation job.       |     
+------------+---------------------------------------------------------------------------------------+

sample_from_ct.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality**
This function takes a regular 3D grid of points, and assigns them amplitudes. It does this by sampling existing medical image data (CT).

+-----------+------------------------------------------------------------------------------------------------+
|**Input**  | :opts:       Options structure for this simulation.                                            |                                                       
|           | :x_size:     Lateral size of the phantom                                                       |                                            
|           | :y_size:     Elevation size of the phantom                                                     |                                              
|           | :z_size:     Axial size of the phantom                                                         |                                           
|           | :z_start:    Distance of the phantom from the transducer                                       |                                                            
|           | :x_g:        Vector of x_coordinates of the points of the input grid                           |                                                                        
|           | :y_g:        Vector of y_coordinates of the points of the input grid                           |                                                                        
|           | :z_g:        Vector of z_coordinates of the points of the input grid                           |                                                                        
+-----------+------------------------------------------------------------------------------------------------+
|**Output** | :multiplicative_amplitude :  Vector of amplitudes assigned to the points of the input 3D grid  |
+-----------+------------------------------------------------------------------------------------------------+

sim_img.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality** 
This function takes a 3D scatter map, and rotates or translates it to simulate the positioning of the US probe relative to the scatter map. It then launches the simulation of the RF line specified by JobAllocator. 

+-----------+------------------------------------------------------------------------------+
|**Input**  | :opts:           Options structure for this simulation                       |  
|           | :probe_angle:    Angle in radians of US beam incidence                       |  
|           | :rf_line:        The number of the RF-line to be simulated                   |       
|           | :y_slice:        Depth of the current slice when simulating a 3D volume      |                   
|           | :amplitudes:     Vector of amplitudes of the scatterers in the scatter map   |                      
|           | :x:              Vector of x-coordinates of the scatterers                   |      
|           | :y:              Vector of y-coordinated of the scatterers                   |      
|           | :z:              Vector of z_coordinated of the scatterers                   |      
+-----------+------------------------------------------------------------------------------+


simulate_rf_line.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality**
This function is in charge of simulating a single RF line.

+-----------+--------------------------------------------------------------------------------------+
| **Input** |   :opts:           Options structure for this simulation                             |  
|           |   :probe_angle:    Angle in radians of US beam incidence                             |  
|           |   :line_no:        The number of the RF-line to be simulated                         |       
|           |   :amplitudes:     Vector of amplitudes of the scatterers in the scatter map         |                      
|           |   :positions:      A N-by-3 matrix of the (x,y,z) coordinates of the N scatterers    |                           
|           |   :y_slice:        Depth of the current slice when simulating a 3D volume            |                   
+-----------+--------------------------------------------------------------------------------------+
|**Output** |:Tx_aperture:    FieldII probe setting (Receive Aperture)                             |
|           |:Rx_aperture:    FieldII probe setting (Transmit Aperture)                            |
+-----------+--------------------------------------------------------------------------------------+

simulation_routine.m
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Functionality** 
This function reads the type of task which has been allocated (t1, t2, or t3), and acts accordingly.

  :t1: Generation of tissue phantoms
  :t2: Simulation of US interaction with tissue phantoms (scatter maps)
  :t3: US b-mode image creation from the simulated data 

+-----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|**Input**  |:simulate:          Flag. Used to decide whether to simulate images or not. Used in debugging to see the result of scatter map generation without simulating its interaction with US|
|           |:fast_run:          Flag. Used in debugging for running the US simulation quickly                                                                                                   |
|           |:evaluation_mode:   Flag. If true, means the program is in evaluation mode, else in dataset generation mode.                                                                        |
|           |:sim2d:             Flag. If true, indicates the program is a 2D simulation (not 3D)                                                                                                |
|           |:new_scatmap:       Flag. If true, indicates that a new random scatter-map can be generated.                                                                                        |
|           |:render_images:     Flag. If true, indicates whether to render the images or not.                                                                                                   |
|           |:job:               job struct containg a description of the allocated simulation job.                                                                                              |
|           |:override_angles:   Flag. If true, indicates that the simulation angle is given in experiment name                                                                                  |
|           |:generate_incl:     Flag. This is a special mode. When set to true, the program generates a scatter map of a circular inclusion                                                     |
|           |:generate_from_ct:  Flag. Whether to use CT images, or the method of random ellipsoids for scatter map generation.                                                                  |
+-----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+