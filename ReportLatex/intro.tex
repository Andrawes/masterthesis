\chapter{Introduction}
\label{ch:introduction}


\section{Ultrasound Simulation: The Big Picture}
Advancements in medical imaging technologies are helping humanity achieve a gargantuan leap in its never ending quest for health and longevity. 
Imaging technologies have become an indispensable tool in medical diagnosis, and thus learning how to use them is a core part of any medical professional's education and training. \\

Traditionally, the way this training is carried out is by having medical students shadow working physicians, and using these imaging technologies on the job, on real patients. For many reasons, this could prove impractical. For instance, how to ensure medical students are exposed to a sufficient number of patients to be able to correctly identify the most rare conditions or illnesses ? \\

This is why in recent times, we have witnessed the appearance of numerical simulators of medical devices, and their increased usage in medical curricula. An example of such a numerical simulator device can be seen in \ref{fig:virtamed_simulator} . These simulators allow future physicians to train on a doll, or a virtual 3D model of a specific body part. These simulators enable controlling the scenarios trainees are exposed to, and ensure a comprehensive training of medical professionals.\\

\begin{figure}[htp!]
    \includegraphics[scale=0.6]{figures/joint_simulator.jpg}
    \centering
    \caption{Knee, shoulder, hip and ankle simulator for arthroscopic skill training \cite{virtamed}}
    \label{fig:virtamed_simulator}
\end{figure}    

One such device being developed is an ultrasound imaging simulator. Ultrasound is indeed the most popular medical imaging technology for its harmlessness, non-invasiveness, affordability and portability. This justifies the need for an ultrasound imaging simulator. The example in Fig. \ref{fig:virtamed_us} shows a system with a probe and a plastic model of the pregnant abdomen. By moving the tracked probe about the model, the screen displays the ultrasound images, called \textbf{B-mode} images, which would be seen by the \gls{sonographer} in a \textit{real} clinical setting, with a live patient. The model of the inside of the body is purely digital, and thus can be edited or replaced. \\

\begin{figure}[htp!]
    \includegraphics[scale=0.12]{figures/best.jpg}
    \centering
    \caption{Gynecological Ultrasound Simulator}
    \label{fig:virtamed_us}
\end{figure}  

\section{The Inverse Problem of Ultrasound Simulation}

The goal in the big picture is to create ultrasound echography simulators which produce images indistinguishable from those obtained using real devices \textit{in vivo}. Current ultrasound simulation techniques suffer from a shortcoming in the realism of the learning experience they provide.\\


The reason simulated ultrasound images often do not look realistic is that they lack certain imperfections, speckles, noise, which are present in real Ultrasound images. An example of this is shown in Fig. \ref{fig:unrealistic}. These artifacts arise due to the sub-wavelength interactions of the ultrasound waves with microscopic elements of the tissue (which absorb and diffract them). The structures in question can be anything from cell membranes to proteins and other molecular compounds. Therefore, in order to enhance the level of realism, these microscopic effects must be taken into account.\\

\begin{figure}[htp!]
    \includegraphics[scale=0.4]{figures/non-homogeneous.png}
    \centering
    \caption{Left panel, simulated ultrasound B-mode image. Right panel, \textit{in vivo} ultrasound B-mode image. Circled in blue is a region where in the simulated image, the texture seems homogeneous, in contrast with the \textit{in vivo} image.Taken from \cite{mattausch}}
    \label{fig:unrealistic}
\end{figure}  

There exists software which can model the interaction of ultrasound with these structures \cite{field2}. However, despite the existence of such software, if the model of the tissue structures in the body is inappropriate, then the resulting B-mode image will not look realistic. The question now becomes, how do we model the tissue of the body, such that when simulated, the system will produce realistic-looking B-mode images? In other terms, what model of the tissue will give us a desired B-mode ultrasound image ? This is called the \textbf{inverse} problem of Ultrasound simulation. In contrast the \textbf{forward} problem consists in simulating an ultrasound (B-mode) image: This is done by taking a representation of the microscopic elements of the tissue, and computing their interaction with the ultrasound waves, in order to produce a B-mode image.\\


The reason why we need the tissue model is that we would like to be able to modify and edit the tissue for our own simulation purposes. Carrying out these modifications directly on the ultrasound B-mode image will not yield realistic results, since it does not take into account the constructive/destructive interactions between ultrasound waves which are deviated by scatterers. An example of this is shown in Fig. \ref{fig:tissue_editing}.\\

The solution to this problem is far from trivial. In chapter \ref{ch:related_work}, we will see how others have tackled this problem. However, the methods we will expose suffer either from computational constraints, or a lack of realism. \\

 \begin{figure}[htb]
    \includegraphics[scale=0.33]{figures/tissue_editing.png}
    \centering
    \caption{Tissue editing directly in the B-mode image domain yields artifacts. The left panel shows a source section which we would like to transplant into a target section. The middle panel shows the target area before tissue editing and the right panel shows the result after editing. Images taken from \cite{mattausch}}
    \label{fig:tissue_editing}
\end{figure}  

\subsection{The Contributions of this Work}

\textbf{How to produce a model of the ultrasound scatterers in the tissue, such that this model can be simulated to look like a real reference ultrasound B-mode image ?} \\

In this project, a novel pipeline is proposed for solving this problem. We adopt a learning-based approach where we use \textbf{Generative Adversarial Networks} (or GANs), to learn how to create a representation of the model of the tissue, from a B-mode image. To the best of our knowledge, this is the first work to tackle the inverse problem of ultrasound simulation with a learning-based approach. \\

Our approach is a supervised learning approach, meaning the neural network must learn to map from B-mode images to tissue scatterer models. This means we need a training dataset consisting of pairs: An ultrasound b-mode image, and its corresponding tissue scatterer model (scatter map). However, such a dataset is not readily available. Therefore, it must be generated. The way this is done is by synthetically creating random tissue models, and then simulating them to obtain the corresponding ultrasound B-mode image, until the dataset is large enough for the network to learn something useful from. Thus a core part of this work studies the issue of generating training data which captures a broad range of visual features. This entire pipeline is illustrated in figure \ref{fig:pipeline}. We argue that our technique can perform faster and generalize better than other published techniques, while producing reliable tissue models from which realistic B-mode images can be obtained. \\

 \begin{figure}[htb!]
    \includegraphics[scale=0.5]{figures/forward_inverse_problem.pdf}
    \centering
    \caption{The \textbf{forward} problem of Ultrasound simulation is marked in blue, while the \textbf{inverse} problem is marked in red}
    \label{fig:pipeline}
\end{figure} 


\subsection{Organization}

The rest of this report is structured in the following fashion: \\
In chapter \ref{ch:related_work}, we expose how others have attempted to address this (and similar) problems and in what way their approaches might not be sufficient or applicable. In chapter \ref{ch:materials_and_methods}, the foundations for an in-depth understanding of the \textbf{forward} and \textbf{inverse} problems of US simulation are laid out, as well as the machine learning theory which underpins the learning algorithms in this work. In chapter \ref{ch:results}, we describe the experiments we carry out, to rigorously evaluate our work for strengths and weaknesses. Finally, in chapter \ref{ch:discussion}, we discuss the strengths and limitations of the proposed methods, and compare them to similar work.\\
