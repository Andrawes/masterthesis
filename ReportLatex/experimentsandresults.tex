\newpage
\chapter{Experiments and Results}
\label{ch:results}

\section{Evaluation Setup and Parameters}

\noindent Experiments are performed on two types of networks. The first network \textbf{SCATGAN1} takes a single B-mode image, and returns a scatter map. The second type of network \textbf{SCATGAN3} takes 3 B-mode images (produced at 3 probe rotation angles $-10^\circ$, $0^\circ$ and $10^\circ$), which are transformed into a single image by concatenating them as the R,G and B channels. \\

\noindent The dataset of \{B-mode image , scatter map \} pairs is divided into a training and evaluation set. For \textbf{SCATGAN1}, the training set is of 120 grayscale images, and the test set is of 30 grayscale images. For \textbf{SCATGAN3}, the training set is of 40 RGB images and the testing set is of 10 RGB images. \\ 

\noindent Our implementation of the pix2pix neural network is largely based on the one found in \cite{pix2pix_github} with the same parameter values. \\

\noindent In order to evaluate the quality of the results at the output of the neural network and to compare to prior work, we carry out a number of evaluations to quantify how robust the network is, its invariance to imaging conditions/probe parameters and its ability to generalize. For \textit{in silico} evaluations, we generate B-mode images with a circular inclusion: we created a numerical phantom with a circular inclusion of 7\,mm radius, centered at 41\,mm from the US probe with an average scatterer amplitude (inside the inclusion) of 10\% of background. We simulated RF data in Field\,II for a linear array imaging at 5\,MHz center frequency. We rotated the probe around the sample to collect 31 unique B-mode images from different viewing directions at angles $\in$ $\{0^\circ, 1^\circ, ..., 30^\circ\}$ (thus giving us 61 such images). The figure \ref{fig:circular_inclusion} shows examples at various probe rotation angles. For \textit{in vivo} B-mode images, we run the \textbf{SCATGAN1} pipeline on actual ultrasound data, and evaluate the resulting B-mode image. We perform 4 types of experiments whose setup is detailed in the subsequent sections and summarized in Tab.~\ref{tab:exp_summary}.\\ 


\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.6]{figures/cystbmode1_-30.png}
	\includegraphics[scale=0.6]{figures/cystbmode1_20.png}
	\includegraphics[scale=0.6]{figures/cystbmode1_0.png}
	\caption{Circular inclusion imaged at various probe rotation angles $-30^\circ$, $20^\circ$ and $0^\circ$}
    \label{fig:circular_inclusion}
\end{figure}


\subsubsection{Invariance Evaluation}
In \textbf{Experiment1}, we run the \textbf{SCATGAN1} network on every one of the 61 B-mode images, thus obtaining 61 scatter maps. Then these resulting scatter maps are all simulated in FieldII at their corresponding angle. Thus there are now 61 B-mode images, which can be compared to 61 ground truth images. This experiment was carried out to evaluate the ability of the network to be invariant to various US imaging parameters (in this case, the parameter tested was probe rotation). Remember that during training, the network has only seen examples at $-10^\circ$, $0^\circ$ and $10^\circ$. Evaluations metrics are applied in the B-mode image domain, and not in the scatterer domain, because two different scatter maps can lead to the same B-mode image. Thus comparing them is ill-defined, while comparing 2 B-mode images makes more intuitive sense.

\subsubsection{Robustness Evaluations}
In \textbf{Experiment2}, we run the \textbf{SCATGAN1} network on \textit{only one} B-mode image (the one at $0^\circ$), and obtain a \textit{single} scatter map. With this single scatter map, we then simulate all the 61 viewing angles, and then compare them to the 61 ground truth images. This is a realistic setting, where a single scatterer representation will be derived for a tissue region to simulate arbitrary viewing angles. This experiment evaluates how robust the output scatter map is, (\textit{i.e} how reliable it is when simulated under different imaging parameters from the B-mode image it was generated from).

\noindent In \textbf{Experiment 3}, we use ScatGAN3 network (three B-mode image input), where the output is a single scatter map which `explains' the three views at the same time. This scatter map is then used to simulate all 61 B-mode images ($-30^{\circ}$ to $+30^\circ$). This experiment was conducted because multiple viewing angles were previously found to bring robustness with optimization based methods~\cite{mattausch}.

\subsubsection{\textit{In vivo} Evaluations}
In \textbf{Experiment 4}, we run the network \textbf{SCATGAN1} on real \textit{in vivo} B-mode images. From this resulting scatterer map, a B-mode image was simulated at the original center frequency of 10~MHz. This experiment evaluates how well our pipeline generalizes beyond our synthetic CT-based dataset, to real \textit{in vivo} B-mode images. Note that \textbf{SCATGAN1} was trained on B-mode data obtained with 5~MHz center frequency.

\begin{table}[htb!]
%\setlength{\tabcolsep}{4pt}
\centering
\caption{Experiment Setup Summary}
\begin{tabular}{ | c | c |c | c | c | c | c | c |}
\hline
Name & Network & \#Inputs & Type & \#scat. maps & \#outputs & Dataset & Purpose \\
\hline
\textbf{Exp1} & \textbf{SCATGAN1} & 61 & \textit{in silico} & 61 & 61 & 150 pairs & Invariance\\
\hline
\textbf{Exp2} & \textbf{SCATGAN1} & 1 & \textit{in silico} & 1 & 61 & 150 pairs & Robustness\\
\hline
\textbf{Exp3} & \textbf{SCATGAN3} & 3 & \textit{in silico} &  1 & 61 &50 pairs (of 3)& Robustness\\
\hline
\textbf{Exp4} & \textbf{SCATGAN1} & 1 & \textit{in vivo} & 1 & 1 & 150 pairs & Generalization\\
\hline
\end{tabular}
\label{tab:exp_summary}
\end{table}


\section{Solving 2D US Images}
\subsection{Single Angle}

\subsubsection{Training Results}
After training the network (\textbf{SCATGAN1}) on paired data, with B-mode images at $-10^\circ$, $0^\circ$ and $10^\circ$, a sample of the network output results on the training set is presented in figure \ref{fig:pix2scat-short}. A more complete version of these results can be viewed in appendix \ref{app:results}. In figure \ref{fig:pix2scat-cyst}, we have results of the scatter maps produced by the network for the circular inclusion. \\  

\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.6]{figures/9644228958318_10-inputs.png}
	\includegraphics[scale=0.6]{figures/9644228958318_10-outputs.png}
	\includegraphics[scale=0.6]{figures/9644228958318_10-targets.png}
	\includegraphics[scale=0.6]{figures/7814950073740_10-inputs.png}
	\includegraphics[scale=0.6]{figures/7814950073740_10-outputs.png}
	\includegraphics[scale=0.6]{figures/7814950073740_10-targets.png}
	\includegraphics[scale=0.6]{figures/7592905384139_-10-inputs.png}
	\includegraphics[scale=0.6]{figures/7592905384139_-10-outputs.png}
	\includegraphics[scale=0.6]{figures/7592905384139_-10-targets.png}
	\caption{Left to right: Input B-mode image, Output scatter map from the network, ground truth}
	\label{fig:pix2scat-short}
\end{figure}

\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.42]{figures/pix2scat_cyst_0.png}
	\includegraphics[scale=0.42]{figures/pix2scat_cyst_-5.png}
	\includegraphics[scale=0.42]{figures/pix2scat_cyst_-10.png}
	\caption{Top to bottom: Reconstruction evaluation of B-mode inclusion images at $0^\circ$, $-5^\circ$ and $-10^\circ$, with heatmap of relative error. Blue is 0\% error, red is 100\% error.}
    \label{fig:pix2scat-cyst}
\end{figure}


\subsubsection{Invariance and Robustness}

\textbf{Experiment1}: In Fig.~\ref{fig:pix2scat-cyst-invariance} are presented the results of the invariance experiment, where the network is run for each ultrasound probe angle from -30 to +30. MII, CNR, SNR, and the $\chi^2$-distance metrics are plotted as a function of rotation angle. For a visual understanding, Fig.~\ref{fig:best_worst_case} shows what the best and worst-performing images (W.R.T the CNR metric) from the Fig.~\ref{fig:pix2scat-cyst-invariance} look like. \\

\begin{figure}[htb!]
	\centering
	\includegraphics[scale=0.6]{figures/pix2scat_invariance_cyst.png}
	\caption{MII, CNR, SNR, $\chi^2$-distance metrics.}
    \label{fig:pix2scat-cyst-invariance}
\end{figure}


\begin{figure}[htb!]
	\centering
	\includegraphics[scale=0.6]{figures/best_case_worst_case.png}
	\caption{Top Row: Best performing case W.R.T CNR metric is at $0^\circ$. Bottom Row: Worst performing case W.R.T CNR metric is at $+9^\circ$.}
    \label{fig:best_worst_case}
\end{figure}


\noindent \textbf{Experiment2}: In figure \ref{fig:pix2scat-cyst-generalization}, are presented the results of the \textit{single-input} robustness experiment, where we obtain the scatter pattern by running the network (\textbf{SCATGAN1}) only once on the $0^\circ$ angle image. A single scatter pattern is obtained, from which a full range of angles are simulated in FieldII. \\ 

\begin{figure}[htb!]
	\centering
	\includegraphics[scale=0.6]{figures/pix2scat_generalization_cyst.png}
	\caption{MII, CNR, SNR, $\chi^2$-distance metrics. Reconstructions are made from a single scatter pattern, obtained from the network at probe angle $0^\circ$}
    \label{fig:pix2scat-cyst-generalization}
\end{figure}

\subsubsection{Evaluation on \textit{in vivo} Data}

\textbf{Experiment4}: We investigate the reconstruction result on \textit{in vivo} data. In figures \ref{fig:pix2scat_in_vivo1} and  \ref{fig:pix2scat_in_vivo2}, a sample of the outputs is shown. It is to be noted that the \textit{in vivo} data was imaged at a center frequency of 10~MHz, whereas the GAN was trained on a dataset imaged solely at 5~MHz. \\

\begin{figure}[htb!]
	\centering
	\includegraphics[scale=0.5]{figures/pix2scat_in_vivo4.png}
	\includegraphics[scale=0.5]{figures/pix2scat_in_vivo1.png}
	\caption{Reconstruction on \textit{in vivo} data}
	\label{fig:pix2scat_in_vivo2}
\end{figure}

\begin{figure}[htb!]
	\centering
	\includegraphics[scale=0.45]{figures/pix2scat_in_vivo3.png}
	\includegraphics[scale=0.45]{figures/pix2scat_in_vivo5.png}
	\includegraphics[scale=0.45]{figures/pix2scat_in_vivo6.png}
	\caption{Reconstruction on \textit{in vivo} data}
    \label{fig:pix2scat_in_vivo1}
\end{figure}


\subsection{Multiple Probe Angles}

\subsubsection{Training Results}

In the work of Mattausch \textit{et al.} \cite{mattausch}, multiple views of the same tissue at different imaging angles are used at once to solve the scatter map. The idea there is that more views will allow for a more robust reconstruction of tissue scatterers. We evaluate here the performance of a network which takes 3 views ($-10^\circ$, $0^\circ$ and $10^\circ$) of the tissue (concatenated in a single image as the RGB channels) at once (\textbf{SCATGAN3}). We apply the same evaluations as the ones used for the single-view case (\textbf{Experiment 2}). Fig.~\ref{fig:3pix2scat_test} shows a sample of the network scatter map outputs from B-mode image inputs coming from the test set. For a more complete view, appendix \ref{app:results} completes the results shown here.\\

\begin{figure}[htb!]
	\centering
	\includegraphics[scale=0.6]{figures/2309707078904-inputs.png}
	\includegraphics[scale=0.6]{figures/2309707078904-outputs.png}
	\includegraphics[scale=0.6]{figures/2309707078904-targets.png}
	\includegraphics[scale=0.6]{figures/2618913200780-inputs.png}
	\includegraphics[scale=0.6]{figures/2618913200780-outputs.png}
	\includegraphics[scale=0.6]{figures/2618913200780-targets.png}
	\includegraphics[scale=0.6]{figures/337112729611-inputs.png}
	\includegraphics[scale=0.6]{figures/337112729611-outputs.png}
	\includegraphics[scale=0.6]{figures/337112729611-targets.png}
	\caption{Left to right: Input B-mode images (concatenated in RGB channels), Output scatter map from the network, ground truth}
	\label{fig:3pix2scat_test}
\end{figure}


\subsubsection{Invariance and Robustness}
% TODO: Add the wilcoxon signed rank test

\textbf{Experiment 3}: As was done in the single view case (\textbf{Experiment 2}), the network is run on an image of the circular inclusion, with the views at $-10^\circ$, $0^\circ$ and $10^\circ$ concatenated as the RGB channels of the image. From this we obtain a single output scatter map. This scatter map is used by FieldII to simulate the round inclusion at 61 different viewing angles. These are the compared to the 61 ground truth B-mode images of the circular inclusion. The results are plotted in figure \ref{fig:3pix2scat-cyst-generalization}. \\ 


\begin{figure}[htb!]
	\centering
	\includegraphics[scale=0.7]{figures/3pix2scat_generalization_cyst.png}
	\caption{MII, CNR, SNR, $\chi^2$-distance metrics. All angle views are simulated from the same scatter map.}
    \label{fig:3pix2scat-cyst-generalization}
\end{figure}


\section{Solving 3D US Volumes}

Using the same technique as earlier (\textbf{SCATGAN1}), we can solve 3D B-mode volumes, by sequentially applying our pipeline to each B-mode slice in a 3D volume. Thus the 3D reconstruction process is linear with the number of (depth-wise) B-mode slices. A slightly different approach was also tried, where we train the network to solve a 3D B-mode volume, rather than a 2D B-mode slice. This experiment yielded interesting results and is detailed in Appendix.~\ref{app:3D_scatterers}.