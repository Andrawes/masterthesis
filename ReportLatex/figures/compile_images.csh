#! /bin/tcsh -f


#find all the .svg files in the directory
foreach i (*.svg)
    set TRUNCATE=`echo $i |sed -n 's/\.svg$//p'`  # remove the .svg extension
    inkscape -f $i -A ${TRUNCATE}.pdf
    echo 'Made PDF: file from' $i 'available as' ${TRUNCATE}.pdf
end
