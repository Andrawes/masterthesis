\chapter{Software Framework}
\label{app:software_framework}


\section{Motivation for a Large Distributed Software Framework}

It has been explained that one of the major requirements for this project is a dataset of \{B-mode image; scatter map\} pairs for the Neural Network to learn from.\\ 
The data generation procedure and pipeline was modified multiple times during the project, and thus at each iteration, a new dataset had to be generated. This would not have been a problem if the dataset generation was not so computationally intensive. To put it into context, the average time it took to generate a single pair on a single core of an Intel Core i7 @~2.80~GHz machine was of 6 h 52 min. A more common scenario during the thesis was similar to simulating 3 or 4 different rotation angles, at up to three depths (for the volume dataset), for a dataset of about 150 image pairs (on average). This sends the compute time to approximately 12.4 thousand hours (or 1.4 years of uninterrupted compute time). This had to be done on average once a week, since changes to the dataset generation pipeline often meant having to re-create the resulting dataset. As a matter of fact, dataset generation was largely the bottleneck in the progression of this work. This is why a principled approach had to be considered.\\ 


\section{Additional Compute Resources}

We have stated the large compute requirements for generating data for this project. The solution to this was to parallelize the software pipeline which generated the data. The problem is that the BIWI compute cluster is not large enough. With a maximum of about 130 single threads using one core each (usually much less depending on how many other tasks are running on the cluster), the BIWI cluster proved insufficient, since at best, it could bring down the dataset generation time (assuming perfect parallelizability) to slightly less than 4 days. This was not fast enough feedback time. For this reason, additional resources were required. \\ 

The ITET department at ETH Zurich provides many computer rooms to students. The computers number around 80 machines (with 4 physical cores each), always turned on and largely un-utilized. This is why un-utilized machines were used in this project for simulation jobs. As part of this thesis, a software toolset was written in order to deploy and run jobs on these computers remotely via SSH. The toolset includes options to : 

\begin{itemize}
	\item Deploy files and software updates to the remote machines
	\item Run tasks remotely 
	\item Select which machines to run on
	\item Monitor and count active tasks
	\item Check if remote machine is being used by another user
	\item Adjust the priority level of the tasks
	\item Suspend or kill remote tasks
	\item Retrieve data produced by remote tasks
	\item Remove and clean remote directories
\end{itemize} 


\section{Description of the Software Framework}
\subsection{Environment Requirements}

Generating a single \{B-mode image; scatter map\} pair can be broken down into the following three steps: \\
\begin{enumerate}
  \item Generation of a random US phantom (scatter map)
  \item Simulation of the RF response of the phantom when subjected to a particular US stimulus from the US probe. This is done line-by-line (called RF lines) independently. There are 205 such lines. This step depends on the previous step being complete. 
  \item Assembling all the RF lines, then processing them to obtain a B-mode image or an image in the RF domain. This step is dependent on the previous step being complete.
\end{enumerate}

Therefore, the software framework for image simulation proceeded in three consecutive steps corresponding to the aforementioned ones. In other terms, in the first step, we generate all the scatter maps required for the dataset. In the second step we simulate all the RF lines for all the phantoms at all the required US beam incidence angles. Finally, we group together all the RF lines corresponding to the same phantom, and create a B-mode image out of them. The reason data pairs are not generated pair-by-pair (such that each thread is responsible for creating a pair) is that if we want to simply generate a single pair, it would run on a single thread and not take advantage of parallelization.\\ 

The second point we address here is how to coordinate the allocation of work among different threads. The constraints we have is that most of the threads cannot access a common storage filesystem (The home directory in ITET department is actually shared, but the storage space is much smaller than the intended size of the dataset). Thus they cannot easily determine what work has already been done, or is currently being carried out by other threads. Thus a different mechanism is put in place to orchestrate threads, such that no inter-thread communication is required.\\


\subsection{JobAllocator}
A master process called \textbf{JobAllocator} is created. Its role is to take care of bookkeeping: It keeps track of all the finished, running and pending jobs in each of the three aforementioned steps. It receives requests from the slave threads (of the fusion cluster) which are free. The request text consists simply of the thread ID of the requesting thread. The response from \textbf{JobAllocator} describes the task which the thread has to run. After the thread has finished running the allocated job, it sends a signal that the job has been finished, and then requests a new one. The \textbf{JobAllocator} application is implemented in Python and Flask (an open-source python micro-framework)\cite{flask} as a RESTful API server (Representational State Transfer Application Programming Interface)\cite{rest_api} which receives and services regular HTTP requests (API calls) from the slave processes. The slave processes are written in MATLAB. This is illustrated in Fig. \ref{fig:software_framework}.\\

\begin{figure}[htb!]
    \includegraphics[scale=0.6]{figures/api.pdf}
    \centering
    \caption{Interaction between slave MATLAB threads and JobAllocator master, using a REST Application Programming Interface.}
    \label{fig:software_framework}
\end{figure}    


