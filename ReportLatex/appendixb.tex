\chapter{Procedural Scatter Map Generation}
\label{app:ellipsoids}

Before making use of CT images when generating random scatter maps for our learning dataset, we tried to generate scatter maps automatically (\gls{procedurally}). The procedural generation of scatter maps was not retained in our pipeline in the end due to its shortcomings. Indeed the approach we tried yielded images which were not diverse enough, nor representative enough for in-vivo data. This appendix is dedicated to detailing our approach to procedural generation of US scatter maps.\\

The approach we used relied on creating ellipsoids in space. 
\begin{enumerate}
	\item We create a number of randomly rotated and scaled ellipsoids in space. 
	\item The center of the ellipsoid is randomly assigned a bright  or dark intensity. Then moving away radially from the center of the ellipsoid increases brightness if the center is dark, or decreases it if the center is bright.
	\item The rate of change of brightness with distance from the ellipsoid center is uniformly random
	\item The various ellipsoidal looking shapes are combined together additively, or multiplicatively. This is also determined randomly.
	\item Any brightness values outside of the [0,1] range are clipped. 
\end{enumerate}

This methods yields images, which when simulated in FieldII, look like the ones in Fig.~\ref{fig:ellipses}.\\  

\begin{figure}[!htb]
    \centering
        \includegraphics[scale=0.4]{figures/1.png}
        \includegraphics[scale=0.4]{figures/2.png}
        \includegraphics[scale=0.4]{figures/3.png}
        \includegraphics[scale=0.4]{figures/4.png}
        \includegraphics[scale=0.4]{figures/5.png}
        \includegraphics[scale=0.4]{figures/6.png}
        \includegraphics[scale=0.4]{figures/7.png}
        \includegraphics[scale=0.4]{figures/8.png}
    \caption{Example of simulated B-mode images generated using the method of superimposed random ellipses}
    \label{fig:ellipses}
\end{figure} 

Despite the images presenting a satisfying amount of variation in terms of visual features, there is very little variation in contrast and brightness levels. Indeed if the histograms of the following images are plotted, we observe 2 dominant modes for the distribution of amplitudes. The first of the modes is at the amplitude 0, the second is for a higher (`bright') intensity. There is also little variation in brightness gradients. Looking at the image examples, we see either homogeneous bright regions, homogeneous dark regions, or abrupt transitions between them.\\
This turns out not to be representative enough of visual features seen in real \textit{in vivo} US images. We come to this hypothesis by training the pix2pix neural network on a dataset generated using this technique. The results however are disappointing for \textit{in vivo} input B-mode images, as the network overfits to the implicit statistical distributions underlying the procedural generation mechanism. An example of this can be seen in Fig.~\ref{fig:failure_case} where it is clear that the network trained on the procedurally generated scatter maps fails to reconstruct structures seen in \textit{in vivo} images.\\

\begin{figure}[!htb]
    \centering
        \includegraphics[scale=0.7]{figures/phantom_failure_case.png}
    \caption{Two examples comparing the performance of trained networks in resolving \textit{in vivo} images. The left window is the input B-mode image. The middle window is the output of a network trained on procedurally generated data. The right window corresponds to the output of a network trained on CT-based training data.}
    \label{fig:failure_case}
\end{figure} 

The procedural dataset generation method can be modified in many ways to yield a richer variety of shapes, intensities, and intensity gradients. However, we abandoned this idea in favor of using real CT images, since they provide (out-of-the-box) a rich enough variety, and also because perfecting the procedural generation technique would require much more time and resources. \\ 

We also tried creating a `fusion' dataset consisting of a 67\%-33\% mixture of respectively procedurally generated and CT-based \{B-mode, scatterer\}-pairs. The proportions were determined by the availability of training data. The network trained on this `fusion' dataset also performs poorly (though not as much as the one with procedural-only data). We hypothesize (without proof or verification) that this is an unprincipled approach because we do not know what should be the correct ratio of procedural vs. CT-based image pairs, such that the training set is not unbalanced (thus resulting in an over-representation of procedurally-generated pairs). One approach which we did not investigate could be to start with a CT-only dataset, then train multiple networks with increasing proportions of procedurally generated pairs, until we find a satisfying ratio. Examples of the network outputs with the fusion dataset are shown in Fig.~\ref{fig:fusion_dataset}. In the first row shows a curious failure example where the network `hallucinates' repetitive structures, and produces a wildly different output from the ground truth. The examples in rows 2 and 3 show a different phenomenon. While the input B-mode images look similar in terms of content and texture, it seems that the network output in the first case (second row) produces a scatter image with a texture similar to the CT-based textures, whereas the in the second case (third row), the texture is more similar to the procedurally-generated one (thus looks closer to the ground truth image). \\

\begin{figure}[htb!]
    \centering
        \includegraphics[scale=0.7]{figures/fusion_dataset.png}
    \caption{Three examples showing the performance of the fusion dataset. The leftmost column shows the input B-mode image. The center column shows the network output (scatter map image), the rightmost column shows the ground truth image scatter map image.}
    \label{fig:fusion_dataset}
\end{figure}