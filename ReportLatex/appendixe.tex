\chapter{Solving 3D Scatterers}
\label{app:3D_scatterers}

The work we have presented focuses on solving the scatterers given a \textit{B-mode image slice}. The question now becomes, how do we solve the scatterers given a B-mode volume (\textit{i.e} spatially adjacent B-mode slices in the y-direction)? Intuitively the first approach is to solve the volume slice-by-slice, by running \textbf{SCATGAN1} on the successive B-mode slices. The scatter map outputs of the pipeline are then also concatenated in a spatially adjacent fashion. We thus have solved the volume. This process is purely linear in the number of B-mode image slices. However, this procedure ignores the fact that an out-of-plane scatterer can still have a visual effect in-the-plane. This is shown in Fig.~\ref{fig:bmode_profile}. We can use the fact that out-of-plane scatterers have a visible impact on the in-plane B-mode image, to our advantage. In this case having multiple adjacent B-mode slices can help us better resolve the y-position of the scatterer in question. \\ 

    \begin{figure}[htb]
        \centering
        \includegraphics[scale=0.8]{figures/bmode_intensity_vs_depth_large.png}
        \caption{B-mode speckle intensity (in [dB]) vs depth in [m] into the plane. The red lines indicate 50\% intensity and 10\% intensity}
        \label{fig:bmode_profile}
    \end{figure} 


To test this intuition, we have performed an experiment. We train a network (we call it \textbf{VOL2SCAT}) on a dataset where 3 spatially adjacent B-mode images are concatenated as the RGB channels of an image. The adjacent B-mode slices are 5.36~mm away from each other ($5.36=2.68\times2$). This has been determined graphically using Fig.~\ref{fig:bmode_profile} by taking into consideration all scatterers which have a brightness contribution of at least 50\% of the maximum brightness observable by an in-plane scatterer (when depth = 0~mm). These are paired with the scatterer image of the central B-mode slice. This training set thus teaches the network to resolve the scatterers for the central slice given its two immediate neighbor slices. Solving an entire B-mode volume is therefore a linear process where we feed the network 3 B-mode slices at once, then shift by one slice and repeat, until the full volume is solved. In Fig.~\ref{fig:vol2scat}, we show a few example outputs of this network, and compare it to the output we would get from \textbf{SCATGAN1} if we use the middle B-mode slice as input B-mode image. We observe encouraging visual results which suggest that using 3 different slices actually does improve the pipeline's ability to solve scatterer volumes. This is given by an increased amount of fine details appearing in the scatterer image, as well as some structures being better resolved, and a higher image sharpness, when compared with the \textit{single-slice} method. These results encourage further quantitative analysis, after simulation in the B-mode domain. In summary, this preliminary experiment seems to confirm our hypothesis that solving volumes instead of individual slices can be beneficial for the reconstruction accuracy of scatter maps. However, further investigations need to be carried out.\\ 


    \begin{figure}[htb]
        \centering
        \includegraphics[scale=0.4]{figures/vol2scat.png}
        \caption{\textbf{VOL2SCAT} performance examples. First column: input B-mode images (concatenated as RGB). Second Column: Scatterer output of VOL2SCAT. Third Column: Scatterer output of \textbf{SCATGAN} (run on the middle B-mode slice only). Fourth column: Ground truth}
        \label{fig:vol2scat}
    \end{figure} 
