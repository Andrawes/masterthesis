\newpage
\chapter{Materials and Methods}
\label{ch:materials_and_methods}

\section{The Forward Problem of Ultrasound Simulation}

Before we go on to explaining in what the \textbf{inverse} problem of Ultrasound Simulation, we will first explain the \textbf{forward} problem of Ultrasound simulation. \\ 

Let $g(l,a,e)$ be a function in $[0,1]^3$. $l$, $a$ and $e$ are the coordinates respectively in the lateral, axial and elevation directions with respect to the ultrasound probe origin. $g(l,a,e)$ is a representation of the tissue we want to simulate under ultrasound. The value of $g(l,a,e)$ can be thought of as an indicator of the strength of the scattering response of the tissue to the stimulus provided by the incoming Ultrasound impulse. \\

We can impose a structure on $g(l,a,e)$ such that it needn't be continuous. We can indeed define $g(l,a,e)$ as set of point scatterers (particles) in space instead of a continuous function of space. We call this the \textbf{scatter map}. We can in this case define $g$ in the following way : \\
\begin{equation}
	g(l,a,e) = \sum_{i=1}^N \delta(s_{l_i}, s_{a_i}, s_{e_i}).a_i
\end{equation} 

where $i \in \{1,2,..,N\}$ is the index associated to each scattering particle. $s_{l_i}$,  $s_{a_i}$ and $s_{e_i}$ are the lateral, axial, and \gls{elevation} coordinates of each point scatterer i. $\delta$ is the Dirac-delta function in $\mathbb{R}^3$. Finally, $a_i \in [0,1]$ is the amplitude associated to the point scatterer $i$. \\

With the structure of the tissue volume explained, the response of this tissue to an Ultrasound impulse $I(l,a)$ in the RF (radio-frequency) domain can be defined as: 

% TODO: write documentation
% TODO: Unify the math notations as in the Overleaf document
% TODO: l,a,e or l,a ? It's not clear.
\begin{equation}
	I(l,a) = g(l,a,e) \ast h(l,a,e) + \gamma(l,a)
\end{equation}

where $\ast$ denotes the convolution between the tissue \textbf{scatter map} $g(l,a,e)$ and $h(l,a,e)$ which is a spatial point spread function (\gls{PSF}), which is the ultrasonic impulse response. Finally, $\gamma(l,a)$ is simply added random noise. In a typical scenario, $h(l,a)$ takes approximately the value of a sinusoidally-modulated Gaussian such that 

\begin{equation}
h(l,a) \approx cos(2\pi f a) . e^{\frac{l^2}{2\sigma_l^2}+\frac{a^2}{2\sigma_a^2}}
\end{equation}

where $f$ is the ultrasound imaging center frequency, $\sigma_l$ and $\sigma_a$ represent the Gaussian spreads in the \gls{lateral} and \gls{axial} directions. The exact form of the PSF is determined by many factors including the geometry of the ultrasound transducer, the focusing distance, the axial distance from the probe, etc. \\ 

In ultrasound simulation, the software package FieldII \cite{field2} is considered state of the art. In this work, all \textbf{forward} ultrasound simulations including the calculation of the ultrasound PSF is done with FieldII. \\

The final step before obtaining an B-mode ultrasound image from $I(l,a)$ will be to perform an envelope extraction followed by logarithmic compression. In particular, this is expressed as follows: \\

\begin{equation}
	B(l,a) = 20 log_{10}(hilbert(I(l,a)))
\end{equation}

where we apply the Hilbert transform in order to extract a positive envelope signal from the RF response $I(l,a)$. The resulting B-mode image $B(l,a)$ can also be further filtered such that regions below a certain amount of gain typically -40~dB are clipped off. \\

% TODO: Add this section
%\section{Generative Adversarial Learning Models to Solve the Inverse Problem}

% TODO: Here we only explain how we do the image to image translation, the GAN was explained already in the previous section

\noindent In this work, the pix2pix GAN Tensorflow model is largely based on an open implementation \cite{pix2pix_github}. The same parameters for the network topology and optimizer are retained. 


\section{A Pipeline for the Generation of Paired Ultrasound Data}

It was explained in the introduction chapter \ref{ch:introduction} that in order to train the GAN to map from ultrasound B-mode images to their underlying scatter pattern, a training dataset is needed. The dataset in question should contain \{B-mode image ; scatter map \} pairs. This dataset is, however, not readily available. It must therefore be generated. This is done in two steps (as illustrated in figure \ref{fig:pipeline} in chapter \ref{ch:introduction}):  
\begin{enumerate}
    \item Generation of a random scatter map.
    \item Simulation of the scatter map in Field II to obtain the B-mode image. 
\end{enumerate} 

\subsection{Generating Random Scatter Maps}

It was explained earlier in this chapter that a scatter map consists of a collection of points in 3D space, each having an amplitude attribute (valued between 0 and 1), which dictates how much of the ultrasound impulse power is scattered back. \\ 

We start the random scatter-map generation process by instantiating the scatterers randomly in a 3D volume, whose height, depth and width are determined by the ultrasound probe's focusing parameters. We instantiate these scatterers such that their average density is of 20~$mm^{-1}$. This number was determined empirically to be a lower bound required to produce realistic-looking B-mode images when the scatterers are simulated.\\ 

These scatterers must then be assigned an amplitude. The amplitude taken by the scatterers determines in the end what structures are seen in the B-mode image, when simulated. These visual features need to present a large enough variation for the GAN to learn from, and generalize to \textit{in vivo} data. As such, it is primordial that the scatter map generation process leads to B-mode images which incorporate a rich diversity of visual features, otherwise, the neural network would overfit to the image statistics arising implicitly from the process of generating synthetic B-mode images. In other words, it would learn to generate scatter maps from the synthetic looking B-mode images of the training set, with a poor ability to generalize (to real \textit{in vivo} data). \\ 

We attempted two different strategies at the scatter map generation process. The first is a fully procedural generation method, however, using it for learning had limited success. However, in short, it consists in creating multiple randomly scaled, randomly rotated ellipsoids. We make them interact with each other in an additive or multiplicative fashion. This generates a variety of random unpredictable shapes such as the ones showcased in figure \ref{fig:ellipses}. It was discovered during our experiments that this method was nonetheless insufficient for generating rich enough variations for the GAN to learn from. More information about it is available in appendix \ref{app:ellipsoids}.\\

\begin{figure}[!htb]
    \centering
        \includegraphics[scale=0.4]{figures/1.png}
        \includegraphics[scale=0.4]{figures/2.png}
        \includegraphics[scale=0.4]{figures/3.png}
        \includegraphics[scale=0.4]{figures/4.png}
        \includegraphics[scale=0.4]{figures/5.png}
        \includegraphics[scale=0.4]{figures/6.png}
        \includegraphics[scale=0.4]{figures/7.png}
        \includegraphics[scale=0.4]{figures/8.png}
    \caption{Example of simulated B-mode images generated using the method of superimposed random ellipses}
    \label{fig:ellipses}
\end{figure} 

The second method proved to be more successful in generalizing to \textit{in vivo} data. It consists of the following procedure:

\begin{enumerate}
    \item Instantiating scatterers in space uniformly at random. 
    \begin{equation}
        s_{l_i} \sim Unif(l_{min}, l_{max}); ~~~~ s_{a_i} \sim Unif(a_{min}, a_{max}); ~~~~ s_{e_i}\sim Unif(e_{min}, e_{max}) ~~~~~~~~ \forall i
    \end{equation}
    \item Assigning them a random amplitude, sampled from a spatially varying normal distribution $\mathcal{N}$.
    \begin{equation}
        a_i \sim \mathcal{N}(\mu(l,a,e), \sigma(l,a,e))
    \end{equation}
    \item The mean $\mu(l,a,e)$ and standard deviation $\sigma(l,a,e)$ at each point in space are controlled by a random image $I$. (Note that because the image exists as discrete pixels, while the scatterers exist in a continuous space, we perform trilinear interpolation).
    \begin{equation}
        \mu(l,a,e) = I(l,a,e)
    \end{equation}
    \begin{equation}
        \sigma(l,a,e) = -\left|\frac{\sigma_{max}-\sigma_{min}}{2}(I(l,a,e)-\frac{1}{2})\right|+\sigma_{max} 
        \label{eq:standard_deviation}
    \end{equation}

    \begin{figure}[!htb]
        \centering
        \includegraphics[scale=0.8]{figures/sigma.png}
        \caption{Sigma function}
        \label{fig:sigma}
    \end{figure} 

    The sigma function is designed with a choice of $\sigma_{min}$ and $\sigma_{max}$ which guarantee that for all values of $I(l,a,e)$, the sampled value of $a_i$ falls in the range [0,1] with probability 90\%. Values of $a_i$ which fall below 0, are clipped to 0, whereas values of $a_i$ falling above 1, are clipped to 1. The reason the value of the $\sigma(l,a,e)$ function tapers down towards the borders of the [0,1] interval, is that when the value of $I(l,a,e)$ and thus $\mu(l,a,e)$ becomes close to 0 or 1, the likelihood of the scatterers falling below 0 or above one become much higher. This is a concern because the clipping process distorts the normal distribution.

    \item This random image $I$, is randomly drawn from a medical imaging dataset (containing CT images). The reason those were chosen is that they naturally present a wide variety of organic visual features, from which our network could learn a general structure.
\end{enumerate}

    The figure \ref{fig:scatter_map_generation} summarizes this entire process of scatter map generation. \\ 

    \begin{figure}[!htb]
        \centering
        \includegraphics[scale=1]{figures/ct_to_scat.pdf}
        \caption{An example of a randomly drawn CT image, transformed into a scatterer map (seen in this image rendered as a dense point cloud flattened along its depth)}
        \label{fig:scatter_map_generation}
    \end{figure}

    To summarize, now two items exist: The randomly drawn medical data image, and the scatter-map point cloud. The former will be placed into training dataset, while the latter will be passed on to the ultrasound simulator as we will see in the next section. (Note that the format accepted by the ultrasound simulator FieldII is a random point cloud with amplitudes assigned to each point, while the format accepted by GAN for training and inference is a standard discrete image). \\


\subsection{Simulating the Scatter Map}
    
    Having devised a process to generate `random' scatter maps, these are later fed into the convolution-based ultrasound simulator FieldII \cite{field2}. This allows us to obtain a simulated ultrasound B-mode image. This completes the dataset generation pipeline. FieldII accepts multiple configuration parameters, which define properties of the ultrasound probe. They are summarized in table \ref{tab:field2_summary}. \\ 


    \begin{table}[htb]
        \centering
        \begin{tabular}{|c|c|}
        \hline
        \centering
        \textbf{Parameter}  & \textbf{Value}  \\ \hline
        Probe Type  & Linear Array \\ \hline
        Transducer Center Frequency [MHz]& 5.0 \\ \hline
        Sampling Frequency [MHz]&  40.0 \\ \hline
        Element Height~[mm] & 3.0 \\ \hline
        Kerf~[mm] & 0.0045 \\ \hline
        \#~Lines & 205 \\ \hline
        Focal Point \{mm,mm,mm\} & {0,0,40} \\ \hline
        Transmit Focus & 25 \\ \hline
        F-number & 1.5 \\ \hline
        \end{tabular}
        \caption{FieldII parameters used in the generation of our data}
        \label{tab:field2_summary}
    \end{table}


    An additional effect to achieve is that of simulating beam incidence from multiple angles onto the same region of interest. There are multiple ways of doing this. First is through ultrasound \gls{beamsteering} as used in \cite{mattausch}. The second method, is to simply simulate rotating the probe around the region of interest. This is illustrated in figure \ref{fig:probe_rotations}. \\ 


    \begin{figure}[!htb]
        \centering
        \includegraphics[scale=0.5]{figures/probe_rotations.pdf}
        \caption{Rotation of ultrasound probe around a rectangular region of interest. The circular intersection denotes the part of the tissue which can be seen from all probe rotations. The rectangular region is where we chose to instantiate random scatterers, such that they can be seen from all probe rotations.}
        \label{fig:probe_rotations}
    \end{figure}

    We achieve this effect by rotating the scatterers with respect to the ultrasound probe when they are subjected to a ultrasound impulse. Then the resulting image is rotated back. Figure \ref{fig:3angles} illustrates the example of the same region of interest imaged at 3 different probe angles ($-10^\circ$, $0^\circ$ and $10^\circ$). \\ 


    \begin{figure}[!htb]
        \centering
        \includegraphics[scale=0.6]{figures/bmode_example_-10.png}
        \includegraphics[scale=0.6]{figures/bmode_example_0.png}
        \includegraphics[scale=0.6]{figures/bmode_example_10.png}
        \caption{Tissue imaged at 3 beam incidence angles by rotating probe at $-10^\circ$, $0^\circ$ and $10^\circ$ from left to right}
        \label{fig:3angles}
    \end{figure}

    Figure \ref{fig:simulated_bmode}, illustrates our data generation pipeline from input to output, resulting in a \{B-mode image, scatter map\} pair. In total, the dataset we generate contains 50 scatter maps simulated at 3 different US probe angles each, thus giving 150 pairs.\\ 

    \begin{figure}[!htb]
        \centering
        \includegraphics[scale=0.85]{figures/scatter_map_example.png}
        \includegraphics[scale=0.85]{figures/bmode_example_0.png}
        \caption{The \textbf{forward} simulation process creates a B-mode image from an image-based representation of a scatter map}
        \label{fig:simulated_bmode}
    \end{figure}



\section{US Image Reconstruction and Performance Metrics}

In order to evaluate the performance of the scatter-pattern generation pipeline, various performance metrics were chosen. 
These performance metrics are applied to a pair consisting of : \\
\begin{enumerate}
	\item An input B-mode image
	\item The simulated B-mode image, resulting from the simulation of the scatter map.
\end{enumerate}

% TODO: Under this section place the noise effect of using different reconstruction samplings for the scatter maps
% TODO: Contrast to noise ratio metric

\subsubsection{Mean Image Intensity}

\begin{equation}
MII = \frac{1}{N}\sum_{i=1}^{N}img[i]
\end{equation}

where $i$ indexes the pixels in the image.

\subsubsection{Root Mean Square Error}

\begin{equation}
RMSE = \frac{\sqrt{\sum_{i=1}^{N}(img_t[i]-img_s[i])^2}}{N}
\end{equation}

where $img_t[i]$ and $img_s[i]$ are respectively the true (reference) image and the reconstruction image.
 

\subsubsection{Signal-to-Noise Ratio}
\begin{equation}
SNR = \frac{\mu}{\sigma}
\end{equation}

where $\mu$ is the mean of the image and $\sigma$ its standard deviation.

\subsubsection{Contrast-to-Noise Ratio}

The contrast-to-noise ratio measures the difference between two regions in the image of different contrasts (typically a dark inclusion, and a brighter background).

\begin{equation}
CNR = \frac{|\mu_{s1}-\mu_{s2}|}{\sigma_{s1}+\sigma{s2}}
\end{equation}

where $\mu_{s1}$ and $\mu_{s2}$ denote the average intensity of the pixels respectively in regions $s1$ and $s2$, corresponding to the bright and dark regions. $\sigma_{s1}$ and $\sigma_{s2}$ denote the standard deviation of the image intensity in those respective regions.

\subsubsection{Incompatibility}

We define a score called incompatibility which measures how dissimilar 2 images are with respect to certain metrics (CNR, SNR, MII). 0\% means the images are identical.

\begin{equation}
Incompatibility[\%] = 100 |{\frac{F_s}{F_t}-1}|
\end{equation}

where $F_t$ and $F_s$ are respectively the values of the metric for the true (reference) image and the reconstruction image.

\subsubsection{Mean Absolute Error}
\begin{equation}
MAE[\%] = 100 \frac{\sum_{i=1}^{N}|img_t[i]-img_s[i]|}{\sum_{i=1}^{N}img_{t}[i]}
\end{equation}

where $img_t[i]$ and $img_s[i]$ are respectively the true (reference) image and the reconstruction image.

\subsubsection{$\chi^2$-Distance Between the Histograms of Images}

\begin{equation}
\chi^{2}(h_s, h_t) = \frac{1}{2}\sum_{l=1}^{D}\frac{(h_s[l]-h_t[l])^{2}}{h_s+h_t}
\end{equation}

where $h_t$ and $h_s$ are respectively the normalized histograms of the true (reference) image and the reconstruction image. $l$ is the index assigned to each bin in the histogram. We obtain these histograms by binning the pixel of the images into $D=50$ bins.  This metric effectively compares the statistics between the two images, by looking at the bin-wise overlap between their histograms.




