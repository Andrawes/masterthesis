\newpage
\chapter{Related Work}
\label{ch:related_work}


\section{Ultrasound Simulation and Scatterer Reconstruction}

This is not the first work which attempts to tackle the issue of ultrasound scatterer reconstruction from ultrasound B-mode images. In Mattausch \textit{et al}\cite{mattausch}, an earlier attempt is made, using a different approach. \\ 

More on the details of convolution-based ultrasound simulation comes in chapter \ref{ch:materials_and_methods}. In short however, to produce a B-mode image, we have to perform a convolution between a certain point spread function, and various points in space acting as spatial impulse signals. This means that essentially, producing B-mode images amounts to performing a convolution with points present in a continuous space. \\

This can be formulated as the following linear equation: 

\begin{equation}
\label{eq:linear}
Ax=b
\end{equation}

where $A$ is the convolution matrix induced by the point spread function, $x$ is a vector of amplitudes corresponding to each point scatterer, and $b$ is the resulting B-mode image. If we call $m$ and $n$ respectively the dimensions of $b$ and $x$, we often are in a case where $n>m$. This means that equation \ref{eq:linear} is under-determined. \\

The \textbf{inverse} problem of this (\textit{i.e.} solving for x given A and b) amounts to performing a deconvolution. But due to the under-determined nature of equation \ref{eq:linear}, the authors of \cite{mattausch} formulate solving for $x$ as an convex optimization problem of the following form:

\begin{equation}
\hat{x} = arg~\min_{x} ||Ax-b||_{1} + \lambda||x||_1 ~~st.~x\geq 0
\end{equation}

where $\lambda$ is a regularization parameter, forcing the amplitudes $x$ of the scatterers to be low. \\ 

We have said that the system of equations \ref{eq:linear} is under-determined. What the authors of \cite{mattausch} do is to image the tissue at multiple angles (via ultrasound beam-steering). Each imaging view of the ultrasound data augments the system of equations \ref{eq:linear}, allowing for a better solution to $x$. 

\begin{equation}
[A_1^T, ..., A_n^T]^Tx = [b_1^T, ..., b_n^T]^T
\end{equation}

where each new view of the tissue adds a convolution matrix $A_i$ and produces a result $b_i$. \\ 

The authors of \cite{mattausch} also evaluate their scatterer reconstruction method, and show that it works on both \textit{in silico} and \textit{in vivo} data. However, there are a few drawbacks in their method: 
\begin{itemize}
    \item The scatterer reconstruction takes a very long time to solve, in the order of hundreds of minutes for simple 2D B-mode images, it is also heavily memory intensive.
    \item The computational constraint in their work prevents them from being able to easily generalize their model to 3D volumes
    \item Their method generates a tissue model which is view-specific, and thus simulating the same tissue model at a different beam-steering angle, or ultrasound probe angle does not yield the desired image. This is why they resort to imaging the same tissue at different angles, and feed all this information at the same time to the solver.
\end{itemize}


In this work, the aforementioned challenges are solved. We use a learning based method which has the following effects:
\begin{itemize}
    \item Inference (to generate tissue models) is run in fractions of a second, as opposed to hundreds of minutes.
    \item The reduced computational constraint allows us to generalize scatterer reconstruction to 3 dimensions, and we explore two techniques of doing so.
    \item The method in this work generates tissue models which are nearly view-independent. 
    \item The method in this work produces results of comparable realism to previous works, even on \textit{in vivo} data.
    \item The method in this work generalizes well to various imaging conditions and probe parameters.
\end{itemize}


% TODO: Cite these
%\cite{tissue_parametrization}
%\cite{alessandrini}


% TODO: Make sure to explain why we do use Cycle GAN
\section{Generative Adversarial Networks}
\subsection{A Brief Overview of Generative Adversarial Networks}

Generative Adversarial Nets \cite{GAN} introduced in 2014 have been a very successful type of neural network architecture. Essentially, they consist of 2 networks: A Generator, and a Discriminator. The generator is a function $G(z)$ which attempts to approximate the distribution of an input dataset. It takes a random input $z\sim P_z(z)$, and maps to it a high-dimensional output which should appear to be sampled from the distribution of the input dataset. Its training is supervised by a second network called a Discriminator. A discriminator is a function $D(x)$ which maps an input $x\sim P_{data}(x)$ to a probability value. This probability indicates the likelihood that the input belongs to the dataset whose distribution is to be approximated. \\

In the case of image datasets, Generators attempt to create images which look similar to the images of a dataset. The output of the Generator is labeled as ``Fake''. Then the Discriminator is fed one of 2 inputs: either a real image taken from the dataset and labeled as ``Real'', or an image produced by the Generator. The output of the discriminator is used to compute the loss which the Generator incurs. The discriminator progressively learns to detect real images from false ones, whereas the Generator learns how to create more and more ``Realistic" images to ``trick the discriminator''. Eventually, the Generator becomes good, so the discriminator may be discarded.\\

Generator and Discriminator functions we have talked about can, and usually are convolutional Neural Networks. More precisely the Generator is usually a partially-strided convolutional Neural Network. A partially strided convolution, also falsely referred to as ``Deconvolution'' is the operation which reverses the effect of convolution, and therefore leads from high-level features to low level features.\\

This can be seen from the perspective of Game Theory as a zero-sum game where the loss of one agent is the profit of the other. Thus the loss function for a GAN as seen in equation \ref{eq:GANloss} is written as: 


% TODO: Make sure the equation is correct for this version, because our equation is for Conditional GAN.


\begin{equation}
	\label{eq:GANloss}
	\min_G\max_D V(D,G) = \mathbb{E}_{x\sim P_{data}(x)}[log(D(x))]+\mathbb{E}_{{z}\sim P_{z}(z)}[log(1-D(G(z)))]
\end{equation}

where the log terms come from the cross-entropy loss. $P_{data}(x)$ is the distribution of the dataset.\\

There are various uses to Generative Adversarial Networks. They have been used to generate visual Art or new musical pieces. They have also been useful in image super-resolution \cite{superr}. Recent works have used them to transform 2D images and sketches into 3D models \cite{3dshape}. 


\subsection{Image-to-Image Translation: Pix2Pix}

Building on the ideas explained in the previous section about Generative Adversarial Networks, the work of Isola \textit{et al.} \cite{pix2pix} defines a particular structure for the Generator Network, the Discriminator network, and the process by which they are trained. \\

Indeed, in the general case of a GAN, a random input is given to the network, from which the network generates an output. What is new here is that the network takes two inputs: The random input, but also an image. This input image is used to condition the behavior of the network at each pixel position. This makes the difference between regular and conditional GANs. In other terms, in a regular GAN $G$

\begin{equation}
 G: z\mapsto y
\end{equation}

where $z$ is a random noise vector and $y$ is an output image. By, contrast, in a conditional GAN (or cGAN): 

\begin{equation}
 G: \{x,z\}\mapsto y
\end{equation}

where x is an input image.\\

This effectively allows the network to perform an image-to-image translation. This is presumably where this network gets its popular name, pix2pix, from. We will explain in chap. \ref{ch:materials_and_methods} why an image-to-image translation framework is an ideal setting for solving the \textbf{inverse} problem of Ultrasound simulation. \\



\subsubsection{Generator Topology}

To begin with, the generator topology (seen in Fig. \ref{fig:generator_unet}) follows the U-NET \cite{unet} model. This is basically an encoder-decoder structure with successive convolutional or deconvolutional layers. What differentiates a U-NET from a regular encoder-decoder are the so-called skip connections. This allows the network to shuttle low-level image details from one end of the network to the other. The input to the generator is an image which we wish to ``translate'' into another.

% TODO: Mention the origin of the below images
\begin{figure}[htp!]
    \includegraphics[scale=0.6]{figures/generator_unet.png}
    \centering
    \caption{Generator: U-NET\cite{unet} topology}
    \label{fig:generator_unet}
\end{figure}   


\subsubsection{Discriminator Topology}

The second piece behind the pix2pix cGAN is the discriminator. The topology in question here is a so-called \textit{patch}GAN. The use of it is motivated by the fact that L1 and L2 losses lead to blurry images in generative contexts. Nonetheless, these losses are accurate for low-frequency details. Therefore, \textit{patch}GAN attempts to reconcile both high and low frequency details. It does this by looking at small image patches and applying L1 on them, instead of applying L1 on the full image. This effectively only penalizes structure mismatch at the scale of the patch. The discriminator is then applied in a sliding manner over the full image. As for the topology of this discriminator (seen in Fig. \ref{fig:patch_gan}), it simply consists of a succession of convolutional layers. Like the generator, it takes two inputs: the template image, and its associated ``translation''. Its output is a guess of how likely the translation is a legitimate one from the training set, or a fake one, produced by the generator.\\

\begin{figure}[htp!]
    \includegraphics[scale=0.33]{figures/patch_gan.png}
    \centering
    \caption{Patch GAN used in the discriminator}
    \label{fig:patch_gan}
\end{figure}   

\subsubsection{Network Training}


The authors of \cite{pix2pix} devise an objective function similar to \ref{eq:GANloss}. The objective function can be broken down into two parts. The first is the cGAN loss : 
\begin{equation}
    \label{eq:cGAN_loss}
    L_{cGAN}(G,D) = \mathbb{E}_{x,y}[log D(x,y)] + \mathbb{E}_{x,z}[log(1-D(x,G(x,z)))]
\end{equation}

which has the same form as \ref{eq:GANloss} but with the conditioning on $x$.

The second term of the loss function is an L1 loss term. 
\begin{equation}
    L_{L1}(G) = \mathbb{E}_{x,y,z}[||y-G(x,z)||_1]
\end{equation}

Thus, putting both terms together yields the final objective: 

\begin{equation}
G^* = arg~\min_G\max_D~[L_{cGAN}(G,D) + \lambda L_{L1}(G)]
\end{equation}


In order to optimize the networks, the authors of \cite{pix2pix} alternate between applying gradient descent on the discriminator then generator. The optimization of the discriminator is slowed down by dividing the value of the objective by 2. This is done to avoid the discriminator from learning too fast, and thus not allowing the generator to evolve. Figure \ref{fig:network_training} illustrates the training procedure for an example scenario where the input image is a drawing, and the desired output is a correctly colored version of this image. \\

\begin{figure}[htp!]
    \includegraphics[scale=0.24]{figures/generator_training.png}
    \includegraphics[scale=0.23]{figures/discriminator_training.png}
    \centering
    \caption{Generator (top) and discriminator (bottom) training procedure in pix2pix. Images from \cite{pix2pix_img}}.
    \label{fig:network_training}
\end{figure}   

One last comment with regards to pix2pix is that the authors \cite{pix2pix} notice that the network is able to learn and generalize with rather small datasets. They show an example where the training set consisted of barely 400 images. In our data-constrained scenario, this is a large advantage.\\

