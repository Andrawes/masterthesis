% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function computes the root mean squared error between two images
%
% INPUT:
%     img1:                                              Input image matrix (ground truth)
%     img2:                                              Input image matrix (reconstruction)
% OUTPUT:
%     root_mean_squared_error:     RMSE

function root_mean_squared_error = rmse(img1, img2)
    squared_error = (img1-img2).^2;
    root_mean_squared_error = sqrt(sum(squared_error(:)))/length(squared_error(:));
end
