% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function computes the mean intensity of an image
%
% INPUT:
%     img:                           Input image matrix 
% OUTPUT:
%     m_intensity:            Mean Image Intensity (MII)

function m_intensity = mean_image_intensity(img)
    m_intensity = mean(img(:));
end
