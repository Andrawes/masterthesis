% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% Obtain the chi-squared distance between the intensity histograms 
% of two images. This script first preprcesses the 2 images by converting
% them to greyscale and equalizing the histograms.
%
% INPUT: 
%   img1:                                   Image matrix
%   img2:                                   Image matrix
%   nbins:                                  Number of historgram bins
%
% OUTPUT:
%   chi_square_distance:   Chi-square distance metric between img
%                                               historgrams

function chi_squared_distance = chi2_intensity_histogram(img1,img2, nbins)

    % Match histograms of the two images
    img1  = imhistmatch(img1, img2, nbins);
    
    
    % Obtain image histogram
    img1_hist = imhist(img1, nbins);
    img2_hist = imhist(img2, nbins);
    
    % Histogram Area under curve
    img1_auc = sum(img1_hist);
    img2_auc = sum(img2_hist);
    
    % Normalize histogram
    img1_hist = img1_hist/img1_auc;
    img2_hist = img2_hist/img2_auc;

    % Begin calculating the chi-squared metric
    intermediate = (((img1_hist-img2_hist).^2)./(img1_hist+img2_hist));
    
    % Obtain positions where both bins have value 0 (to avoid division-by-zero-problems)
    zero_mask = (img1_hist == 0 & img2_hist ==0);
    % Set all NaNs resulting from division-by-zero, to zero.
    intermediate(zero_mask) = 0;
    
    % FInish computing the chi-squared distance
    % Note: In some images, there is a large spurrious mismatch in the very
    % first few bins, which causes the X-squared metric to explode.
    % So we ignore these first three bins which are usually problematic
    % and add to our metric 3x the average amount of intermediate error 
    % per bin, to compensate for the bins which we removed.
    chi_squared_distance = 0.5*(sum(intermediate(4:end))+3*mean(intermediate(:)));
    
%       if chi_squared_distance > 0.02  
%         figure(7) 
%         title('pre histmatch')
%         plot(imhist(img1, 50))
%         hold on
%         plot(imhist(img2, 50))
%         hold off
%         pause(1)
%         figure(6)
%       end  
    
end
