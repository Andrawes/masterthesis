% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function computes the signal to noise ratio of an image
%
% INPUT:
%     img:               Input image.
% OUTPUT:
%     s2n_ratio:    Signal to noise ratio

function s2n_ratio = snr(img)
    img = double(img);
    mu = mean(img(:));
    sigma = std(img(:));
    s2n_ratio = mu/sigma;
end
