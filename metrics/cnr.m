% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function computes the contrast to noise ratio of an image 
% It does this by evaluating the mean and standard deviation of the 
% pixel intensities in two box_shaped regions: A bright one and a 
% dark one. 
%
% INPUT: 
%   img:                                Image matrix
%   bright_center_x:         Location of the bright region bounding 
%                                           box center in the lateral direction  [px]                            
%   bright_center_z:         Location of the bright region bounding 
%                                           box center in the axial direction [px]
%   dark_center_x:           Location of the dark region bounding 
%                                           box center in the lateral direction [px]
%   dark_center_z:           Location of the dark region bounding 
%                                           box center in the axial direction [px]
%   box_size:                     Bounding box (square) length. [px]
%
% OUTPUT:
%   c2n_ratio:                    Contrast-to-noise ratio       

function c2n_ratio = cnr(img, bright_center_x, bright_center_z, dark_center_x, dark_center_z, box_size)
   
    % Calculate half the box size
    h_box = round(box_size/2);
    
    % Extract relevant image regions
    bright_region = img(bright_center_z - h_box:bright_center_z+h_box , bright_center_x - h_box:bright_center_x+h_box ); 
    dark_region = img(dark_center_z - h_box:dark_center_z+h_box , dark_center_x - h_box:dark_center_x+h_box ); 
    
    % Calculate the means 
    mean_bright = mean(bright_region(:));
    mean_dark = mean(dark_region(:));
    
    % Calculate the standard deviations
    stdev_bright = std(double(bright_region(:)));
    stdev_dark = std(double(dark_region(:)));
    
    % Calculate Contrast to noise ratio
    c2n_ratio = abs(mean_bright-mean_dark)/(stdev_bright+stdev_dark);
    
    %figure(20)
    %imshow(bright_region)
    %figure(21) 
    %imshow(dark_region)
    %pause(5)
end
