% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function computes the mean intensity of an image
%
% INPUT:
%     img:                           Input image matrix 
% OUTPUT:
%     m_intensity:            Mean Image Intensity (MII)

% This function computes the root mean squared error between two images
function root_mean_squared_error = rmse(img1, img2)
    squared_error = (img1-img2).^2;
    root_mean_squared_error = sqrt(sum(squared_error(:)))/length(squared_error(:));
end
