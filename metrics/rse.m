% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function computes the relative squared error between 2 images. 
% The first image is the ground truth, the second image is the reconstruction 
%
% INPUT:
%     img_gt:                                   Input image matrix (ground truth)
%     img_recon:                            Input image matrix (reconstruction)
% OUTPUT:
%     relative_squared_error:     Relative squared error

function relative_squared_error = rse(img_gt, img_recon)
    squared_error = (img_gt-img_recon).^2;
    root_squared_error = sqrt(sum(squared_error(:)));
    gt_squared = img_gt.^2;
    denominator = sqrt(sum(gt_squared(:)));
    relative_squared_error = root_squared_error/denominator ;
end
