function [fbmode] = FilterBMode(bmode,width,fmode)
%FilterBMode Filter a B-mode image 
%   Detailed explanation goes here
if fmode==0
    h=gausswin(width);
    H=h*h';
    N=H./sum(sum(H));
    fbmode=filter2(N,bmode);
else
    fbmode=medfilt2(bmode,[width width]);
end
end

