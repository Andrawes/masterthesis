% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% Sometimes black and white images are encoded as 3-channel 
% images where the R, G, and B channels have the same values. 
% This function returns a grascale image (single channel).
%
% INPUT:
%     img1:      Input image.
% OUTPUT:
%     img1_chan: Single channel version of the input image.

function img1_chan = single_chan(img1)
    if ndims(img1) == 3
        img1_chan = img1(:,:,1);
    else
        img1_chan = img1;
    end
end
