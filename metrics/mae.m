% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This function computes the mean absolute error between two images 
%
% INPUT:
%     img1:                                              Input image matrix (ground truth)
%     img2:                                              Input image matrix (reconstruction)
% OUTPUT:
%     mean_absolute_error:               MAE

function mean_absolute_error = mae(img1, img2)
    
    img1 = double(img1);
    img2 = double(img2);
    
    % remap intensity values to to B-mode range
    cutoff_db = -40;
    max_db = -5;
    img1 = img1/(max(img1(:)));
    img1 = img1*(max_db-cutoff_db);
    img1 = img1+max_db;
    img2 = img2/(max(img2(:)));
    img2 = img2*(max_db-cutoff_db);
    img2 = img2+max_db;

    % Logarithmic decompression (obtain envelope)
    mgain = 20;
    offset = 0.0;
    env1 = 10.^(img1/mgain)-offset;
    env2 = 10.^(img2/mgain)-offset;
    
    % New method (Oliver's method)
    fw=20;
    mean_in_1=mean(env1(:));
    mean_out_1=mean(env2(:));
    img1_f=FilterBMode(env1,fw,0);
    img2_f=FilterBMode(env2,fw,0)*mean_in_1/mean_out_1;
    mean_absolute_error=sum(abs(img1_f(:)-img2_f(:)))./sum(img1_f(:));
    mean_absolute_error = mean_absolute_error*100;


%    %Old method (Andy's method)
%    range = max(img1(:)) - min(img1(:));
%
%    % Brightness equalization 
%    img1 = (100-mean(img1(:)))/numel(img1)+img1;
%    img2 = (100-mean(img2(:)))/numel(img2)+img2;
%
%    absolute_error = abs(img1-img2);
%    mean_absolute_error = mean(absolute_error(:));
%    
%    % As a percentage of the full range of intensity values
%    mean_absolute_error = 100*mean_absolute_error/range;  
end
