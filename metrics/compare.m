% Andrawes Al Bahou (c) - ETH Zurich - 2018
%
% FUNCTIONALITY:
% This script calculates certain metrics on pairs of images. 
% The pairs of images are assumed to be named identically 
% and have the same dimensions. 
% This script calculates certain metrics on these images, and 
% then computes a percentage fit between the 2 images of the
% pair, for each metric. 
% Image pairs are assumed to be placed in 2 different directories
% and be named identically.
% The output file is an html table (visible using any web browser)
%
% INPUT: 
%   gt_dir:                 Ground truth images directory
%   rec_dir:               Reconstructed images directory
%   results_path:     Relative path where to store the html file
%
% OUTPUT:
%   An html file summarizing all the results for all compared
%   pairs of b-mode images


%=======================================================
% User configuration
%=======================================================
% Ground truth dir and reconstruction dir (pix2scat invariance)%
%gt_dir = '../validation_images/cyst_bmode_5mhz_m30_p30_10perc';
%rec_dir = '../validation_images/cyst_5mhz_m30_p30_10perc_reconstructed_pix2scat';

% Ground truth dir and reconstruction dir (pix2scat invivo 5mhz)
%gt_dir = '../validation_images/random_in_vivo';
%rec_dir = '../validation_images/random_in_vivo_reconstructed';

% Ground truth dir and reconstruction dir (pix2scat invivo 10mhz)
%gt_dir = '../validation_images/random_in_vivo';
%rec_dir = '../validation_images/random_in_vivo_10mhz';

% Ground truth dir and reconstruction dir (pix2scat generalization)
%gt_dir = '../validation_images/cyst_bmode_5mhz_m30_p30_10perc';
%rec_dir = '../validation_images/cyst_5mhz_reconstruction_m30_p30_generalization';

% Ground truth dir and reconstruction dir (3pic2scat (scatgan3) robustness)
gt_dir = '../validation_images/cyst_bmode_5mhz_m30_p30_10perc';
rec_dir = '../validation_images/cyst_5mhz_3chan_train_m10_p10_reconstruction_m30_p30';

% Optimization over in-vivo-4
%gt_dir = '../validation_images/random_in_vivo';
%rec_dir = '/scratch/aalbahou/data/newdata3';

% Breast inclusion
%rec_dir = 'blablaoutputs';
%gt_dir= 'blablaref';

% Report of the results html file output dir
results_path = 'histogram.html';
% This flag is set in cyst_sweep mode to plot the metrics as a function
% on angle
sweep = true ;

% If set to true, compares only one reference image to many reconstructions
one_to_many = false;

% When one-to-many is set to true, must specify the name of the reference
% image. Otherwise this variable  is ignored
ref_im_name = '3.png';

% Image display scaling factor (between 0 and 1)
scaling_factor = 1;
%=======================================================


% Initialize strings for the HTML file
html_header = '<html><body><table>';
html_end = '</table></body></html>';
html_string = ''; % The string containing the full html file

% Name of the files
files = dir(strcat(rec_dir,'/*.png'));

% Iterate over all pairs of images
for k=1:length(files)
    
    % Initialize strings for the HTML file
    table_header = '<tr>';
    table_MII = '<tr><td>MII</td>';
    table_CNR = '<tr><td>CNR</td>';
    table_SNR = '<tr><td>SNR</td>';
    table_chisq = '<tr><td>CHISQ</td>';
    table_MAE = '<tr><td>MAE</td>';
    table_RMSE = '<tr><td>RMSE</td>';

    % extract the current angle
    if sweep == true
        infor = regexp(files(k).name,'\.','split');
        infor = char(infor(1));
        infor = regexp(infor,'_','split');
        infor = char(infor(2));
        current_angle = str2num(infor);
        index_position = current_angle +31;
    end

    % Image 1 is the reference imge, while image 2 is the reconstruction
    if one_to_many == true
        img1_dir = strcat(gt_dir,'/',ref_im_name);
    else
        img1_dir = strcat(gt_dir, '/' ,files(k).name);
    end
    img2_dir = strcat(rec_dir, '/' ,files(k).name);
    
    % Difference image dir (timestamp the files)
    dt = datestr(now,'mmmm-dd-HH-MM');
    img_diff_dir = strcat('diff', '/',dt,'_',files(k).name);

    % Read the pair of images
    img1 = imread(img1_dir);
    img2 = imread(img2_dir);
    
    % If the image is written as three channels, just select one of the
    % channels
    img1 = single_chan(img1);
    img2 = single_chan(img2);

    % Write Difference image
    gaussian_sigma = 1;
    diff_img = imgaussfilt(double(img1),gaussian_sigma)-imgaussfilt(double(img2), gaussian_sigma);
    diff_img = diff_img(:,:,1);
    diff_img = diff_img/255;
    diff_img = abs(diff_img);
    
    
    % Create and save difference figure
    imagesc(diff_img);
    set(gca,'XTick',[]) % Remove the ticks in the x axis!
    set(gca,'YTick',[]) % Remove the ticks in the y axis
    set(gca,'Position',[0 0 1 1]) % Make the axes occupy the hole figure
    colormap(jet)
    colorbar
    caxis([0 1])
    saveas(gcf,img_diff_dir,'png')
        
    % Image dimensions
    size_img = size(img1);
    image_width = size_img(2)*scaling_factor;
    image_height = size_img(1)*scaling_factor;

    % Add data to HTML table header
    table_header = strcat(table_header,"<td><img src='",img1_dir,"' width='",num2str(image_width),"' height='",num2str(image_height),"'></td>");
    table_header = strcat(table_header,"<td><img src='",img2_dir,"' width='",num2str(image_width),"' height='",num2str(image_height),"'></td>");
    % Placeholder
    table_header = strcat(table_header,"<td><img src='",img_diff_dir,"' width='",num2str(image_width),"' height='",num2str(image_height),"'></td>");
    
    % Mean Image Intensity
    mii1 = mean_image_intensity(img1);
    mii2 = mean_image_intensity(img2);
    mii_comp = comp(mii1, mii2);
    table_MII = strcat(table_MII,'<td>',num2str(mii_comp),' % (' ,num2str(mii1),'/',num2str(mii2) ,') </td>');

    % Signal to Noise Ratio
    snr1 = snr(img1);
    snr2 = snr(img2);
    snr_comp = comp(snr1,snr2);
    table_SNR = strcat(table_SNR,'<td>',num2str(snr_comp),' % (' ,num2str(snr1),'/',num2str(snr2) ,') </td>');

    % Contrast to noise Ratio
    cnr1 = cnr(img1,131,189,131,273,64);
    cnr2 = cnr(img2,131,189,131,273,64);
    cnr_comp = comp(cnr1, cnr2);
    table_CNR = strcat(table_CNR,'<td>',num2str(cnr_comp),' % (' ,num2str(cnr1),'/',num2str(cnr2) ,') </td>');

    % Root mean square error
    rms_error = rmse(img1,img2);
    table_RMSE = strcat(table_RMSE, '<td>', num2str(rms_error),'</td>');

    % Chi-square distance between intensity histograms
    chisq = chi2_intensity_histogram(img1, img2, 50);
    table_chisq = strcat(table_chisq, '<td>', num2str(chisq),'</td>');
    
    % Mean absolute error
    ma_error = mae(img1,img2);
    table_MAE = strcat(table_MAE, '<td>', num2str(ma_error),' %','</td>');
    
    if sweep ==true
        metrics(index_position).mii_comp = mii_comp;
        metrics(index_position).snr_comp = snr_comp;
        metrics(index_position).cnr_comp = cnr_comp;
        metrics(index_position).rms_error =  rms_error;
        metrics(index_position).chisq = chisq;
        metrics(index_position).ma_error = ma_error;
    end
    
    % Complete the ends of the HTML strings.
    table_header = strcat(table_header,'</tr>');
    table_MII = strcat(table_MII,'</tr>');
    table_CNR = strcat(table_CNR,'</tr>');
    table_SNR = strcat(table_SNR,'</tr>');
    table_chisq = strcat(table_chisq,'</tr>');
    table_MAE = strcat(table_MAE,'</tr>');
    table_RMSE = strcat(table_RMSE,'</tr>');
    blank_line = '<tr><td></td></tr>';

    % Form the HTML file as a string
    if sweep == true % Include CNR
        html_string = strcat(html_string,table_header, table_MII, table_SNR, table_CNR, table_chisq, table_MAE, table_RMSE, blank_line);
    else
        html_string = strcat(html_string,table_header, table_MII, table_SNR, table_chisq, table_MAE, table_RMSE, blank_line);
    end
    
end

% Add html header and footer
html_string = strcat(html_header, html_string, html_end);

% Write the HTML file
fileID = fopen(results_path,'w');
fprintf(fileID, '%s' ,html_string);
fclose(fileID);

% Plot the evolution of various metrics with angle.
if sweep ==true 
    fig = figure();
    subplot(2,1,1)
    plot([-30:1:30], [metrics.mii_comp]);
    hold on;
    plot([-30:1:30], [metrics.snr_comp]);
    hold on;
    plot([-30:1:30], [metrics.cnr_comp]);
    title('MII, CNR, SNR')
    legend('MII', 'SNR', 'CNR')
    xlabel('Probe Rotation [in^\circ]')
    ylabel('Incompatibility [%]')
    grid on;
    grid minor;
    
    subplot(2,1,2)
    plot([-30:1:30], [metrics.chisq]*1000);
    title('\chi^2 - distance between image histograms')
    xlabel('Probe Rotation [in^\circ]')
    ylabel('\chi^2 - distance [\times10^{-3}]')
    grid on;
    grid minor;
end

% signrank test on the mii metric
% [p,h] = signrank([metrics_pix2scat_generalization.mii_comp], [metrics_3pix2scat.mii_comp])
