% This script computes the compatibility between 2 images, 
% as a function of a metric between them. 
% The formula for compatibility [%] is : 
% 100 *|F_s/F_t-1|
% f_t and f_s are scalars.
function compatibility = comp(f_t, f_s)
    compatibility = 100*abs((f_s./f_t)-1);
end