% Andrawes Al Bahou (c) - ETH Zurich - Oct 2018
% FUNCTIONNALITY : 


% Exctract middle channel from a 3-cannel image


%============================================================
% Setup
%============================================================

% List of relevant directories 
bmode_dir = '/scratch_net/biwidl103/aalbahou/training_results/vol2scat_ct_oct/vol_ct_5mhz/val';
output_dir = '/scratch_net/biwidl103/aalbahou/training_results/vol2scat_ct_oct/middle_channel';
%============================================================


% Create output folder
if ~exist(output_dir,'dir')
        mkdir(output_dir);
end


% Scan over all bmode images
files = dir(strcat(bmode_dir,'/','*.png'));   
for file = files'

    % Read image
    channel = double(imread(strcat(bmode_dir,'/',file.name)));
    bmode = channel(:,:,2);

    % Scale values down to interval between [0 and 1]
    bmode = bmode/(max(bmode(:)));

    % Write image to file 
    imwrite(repmat(bmode,1,1,3), strcat(output_dir,'/',file.name));

end
