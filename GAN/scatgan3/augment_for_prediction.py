# This script augments the image data into the format accepted 
# by the network. This means we concatenate a black image of the 
# same dimensions to the right of the current image.

import numpy as np
import cv2
import os
import sys
import shutil


input_dir = "image_new/"
output_dir = "image_new/"

for file in os.listdir(input_dir):
	if file.endswith(".png"):
		base_name = file.split('.')[0]
		file_dir = input_dir+base_name+".png"
		image = cv2.imread(file_dir)
		#image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		#image = cv2.equalizeHist(image)
		new_img = np.zeros((image.shape[0], 2*image.shape[1], 3))
		new_img[:,0:257,:] = image

		cv2.imwrite(output_dir+base_name+'_augmented.png',new_img)
